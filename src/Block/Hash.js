import React from "react";
import PropTypes from "prop-types";
import Tag from "antd/lib/tag";
import { isValidHashDifficulty } from "../Cards/MineCard/util.js";

const Hash = props => {
  return (
    <div 
      style={{ 
        display: "flex",
        justifyContent: "space-between",
        flexWrap: "wrap",
        maxWidth: "100%",
        overflow: "auto"
      }}
    >
      <div style={{ marginRight: "15px", fontWeight: "500" }}>Хэш</div>
      <Tag
        color={hashColor(props.hash)}
        style={{
          display: "block",
          fontSize: "9pt",
          fontFamily: "Courier New",
          cursor: "default",
          maxWidth: "100%",
          overflow: "auto"
        }}
      >
        {props.hash}
      </Tag>
    </div>
  );
};

export function hashColor(hash) {
  if (isValidHashDifficulty(hash)) {
    return "green";
  } else {
    return "red";
  }
}

Hash.propTypes = {
  hash: PropTypes.string
};

export default Hash;



// WEBPACK FOOTER //
// ./src/Block/Hash.js