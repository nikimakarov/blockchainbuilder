import React from "react";
import Button from "antd/lib/button";
import Tag from "antd/lib/tag";
import { reMine } from "../actions.js";
import { isValidHashDifficulty } from "../Cards/MineCard/util.js";
import { calculateHash } from "../Cards/MineCard/util.js";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const NonceButton = props => {
  const { index, previousHash, timestamp, nonce, transactions, peer } = props;

  if (isValidHashDifficulty(props.hash)) {
    return <Tag style={{ cursor: "default" }}>{nonce}</Tag>;
  } else {
    return (
      <Button
        shape="circle"
        size="large"
        icon="tool"
        style={{
          background: "linear-gradient(45deg, #00c6ff, #0072ff)",
          color: "white",
          border: "transparent",
          boxShadow: "0 7px 14px rgba(50,50,93,.1), 0 3px 6px rgba(0,0,0,.08)"          
        }}
        onClick={() => {
          const newBlock = regenerateBlock(index, previousHash, timestamp, transactions)

          props.reMine(
            index,
            peer,
            newBlock.nonce,
            newBlock.hash,
            newBlock.timestamp
          )
        }}
      />
    );
  }
};

NonceButton.propTypes = {
  nonce: PropTypes.number,
  hash: PropTypes.string,
  reMine: PropTypes.func
};

function mapDispatchToProps(dispatch) {
  return {
    reMine: (index, peer, nonce, hash, timestamp) => {
      dispatch(reMine(index, peer, nonce, hash, timestamp));
    }
  };
}

const NonceButtonContainer = connect(null, mapDispatchToProps)(NonceButton);

export default NonceButtonContainer;

function regenerateBlock(index, previousHash, timestamp, transactions) {
  let nonce = 0;
  let hash;
  // proof-of-work
  do {
    timestamp = new Date().getTime();
    nonce = nonce + 1;
    hash = calculateHash(index, previousHash, timestamp, transactions, nonce);
  } while (!isValidHashDifficulty(hash));

  return {
    nonce,
    timestamp,
    hash
  };
}



// WEBPACK FOOTER //
// ./src/Block/NonceButton.js