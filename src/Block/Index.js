import React from 'react';
import PropTypes from 'prop-types';

const timeStyle = {
  fontWeight: "300",
  fontSize: "8pt"
}

const indexStyle = {
  fontWeight: "400"
}

const Index = props => {
  const timestamp = <span style={timeStyle} >Время создания: {new Date(props.timestamp).toLocaleString('ru')}</span>
  return (
    <div style={{ fontSize: "24px", whiteSpace: "nowrap", overflow: "auto" }}>
      {props.index === 0 ? (
        <span style={{ ...indexStyle, letterSpacing: "1px" }}>БЛОК ГЕНЕЗИСА</span>
      ) : (
        <span style={{ ...indexStyle, letterSpacing: "1px" }}>БЛОК #{props.index}</span>
      )}{" "}
      <span style={{ letterSpacing: ".5px" }}>{timestamp}</span>
    </div>
  )
};


Index.propTypes = {
  index: PropTypes.number
};

export default Index;


// WEBPACK FOOTER //
// ./src/Block/Index.js