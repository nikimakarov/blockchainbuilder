import React from "react";
import Tag from "antd/lib/tag";
import PropTypes from "prop-types";
import { hashColor } from "./Hash.js";

const PreviousHash = props => {
  if (props.index === 0) {
    return null;
  } else {
    return (
      <div
        style={{
          marginTop: "15px",
          marginBottom: "7px",
          display: "flex",
          justifyContent: "space-between",
          flexWrap: "nowrap",
          maxWidth: "100%",
          overflow: "auto"
        }}
      >
        <div style={{ whiteSpace: "nowrap", fontWeight: "500" }}>{'Хэш предыдущего блока'}</div>
        <Tag
          color={hashColor(props.previousHash)}
          style={{
            fontSize: "8pt",
            float: "right",
            fontFamily: "Courier New",
            cursor: "default",
            display: "block",
            background: "none",
            borderColor: "transparent"
          }}
        >
          {props.previousHash}
        </Tag>
      </div>
    );
  }
};

PreviousHash.propTypes = {
  index: PropTypes.number,
  previousHash: PropTypes.string
};

export default PreviousHash;



// WEBPACK FOOTER //
// ./src/Block/PreviousHash.js