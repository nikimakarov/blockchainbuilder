import React from 'react';
import Card from "antd/lib/card";
import Hash from "./Hash.js";
import PreviousHash from "./PreviousHash.js";
import NonceButton from "./NonceButton.js";
import Index from "./Index.js";
import TransactionsTable from "../TransactionTable/TransactionsTable.js"
import "./Block.css";

const Block = props => {
  return (
    <Card
      className={`${props.index === 1 ? "block-first" : ""} block block-shadow`}
    >
      {/* Transactoins */}
      {props.transactions.length === 0 ? null : (
        <TransactionsTable
          blockIndex={props.index}
          currentPeer={props.currentPeer}
          data={props.transactions}
          blockchain={props.blockchain}
        />
      )}

      {/* Previous Hash */}
      <PreviousHash previousHash={props.previousHash} index={props.index} />
      
      {/* Hash */}
      <Hash hash={props.hash} />

      {/* Block, and Mine */}
      <div
        style={{
          marginTop: "27px",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          flexWrap: "nowrap"
        }}
      >
        <Index index={props.index} timestamp={props.timestamp} />

        <NonceButton
          hash={props.hash}
          nonce={props.nonce}
          index={props.index}
          timestamp={props.timestamp}
          peer={props.currentPeer}
          transactions={props.transactions}
          previousHash={props.previousHash}
        />
      </div>
  </Card>
  );
};

export default Block;