import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';
import { Provider } from 'react-redux';
import { unregister } from './registerServiceWorker';
import { PersistGate } from 'redux-persist/integration/react';
import config from './Utils/configureStore';
console.log(config.store);
console.log(config.persistor);
ReactDOM.render(<Provider store={config.store}>
                    <PersistGate loading={null} persistor={config.persistor}>
                        <App/>
                    </PersistGate>
                </Provider>, document.getElementById('root'));
unregister();