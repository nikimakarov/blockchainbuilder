import {
  ADD_PEER,
  REMOVE_PEER,
  ADD_BLOCK,
  MUTATE_ADDRESS,
  MUTATE_AMOUNT,
  RE_MINE,
  ADD_HISTORY,
  ADD_TRANSACTION,
  REMOVE_TRANSACTION,
  CONNECT_PEER,
  DISCONNECT_PEER,
  REPLACE_CHAIN,
  REPLACE_TRANSACTIONS
} from "./actions";
import { calculateHash } from "./Cards/MineCard/util.js";
import { combineReducers } from "redux";
import { generateName } from "./Utils/util.js";

export function genesisBlock() {
  return {
    index: 0,
    previousHash: "0",
    timestamp: 1508270000000,
    transactions: [],
    hash: "000231f48b131b1c721b508434cdbf308b0ae7c1a7f6a5ed7db56a34c76c9530",
    nonce: 2176
  };
}

function node(existingNames) {
  return {
    peer: generateName(existingNames),
    blockchain: [genesisBlock()],
    connectedPeers: [],
    transactions: []
  };
}

const node2Name = generateName([]);

const initialNode = {
  peer: "Satoshi",
  blockchain: [genesisBlock()],
  connectedPeers: [{ name: node2Name, messages: [] }],
  transactions: []
};

const initialNode2 = {
  peer: node2Name,
  blockchain: [genesisBlock()],
  connectedPeers: [{ name: "Satoshi", messages: [] }],
  transactions: []
};

// block reducer
function blockchain(state = [initialNode, initialNode2], action) {
  switch (action.type) {
    case ADD_PEER:
      const existingNames = state.map(node => node.peer);
      return [...state, node(existingNames)];

    case REMOVE_PEER:
      const removePeer = state.filter(node => node.peer !== action.peer);
      return removePeer.map(node => {
        node.connectedPeers = node.connectedPeers.filter(
          peer => peer.name !== action.peer
        );
        return node;
      });

    case ADD_BLOCK:
      return state.map(node => {
        if (node.peer === action.peer) {
          node.blockchain = [...node.blockchain, action.block];
        }
        return node;
      });

    case ADD_TRANSACTION:
      return state.map(node => {
        if (node.peer === action.peer) {
          node.transactions = [...node.transactions, action.transaction];
        }
        return node;
      });

    case REMOVE_TRANSACTION:
      return state.map(node => {
        if (node.peer === action.peer) {
          node.transactions = node.transactions.filter(
            transaction => {
              return JSON.stringify(transaction) !== JSON.stringify(action.transaction)
            }
          );
        }
        return node;
      });

    case REPLACE_TRANSACTIONS:
      return state.map(node => {
        if (node.peer === action.peer) {
          node.transactions = action.transactions;
        }
        return node;
      });

    case MUTATE_ADDRESS:
      /*mutate the block // only be able to mutate unspent transactions
      peer, index of the block, transaction, index, output, index;*/
      return state.map(node => {
        const { peer, blockIndex, txIndex, outputIndex, address } = action;
        if (node.peer === peer) {
          node.blockchain = node.blockchain.map((block, index) => {
            if (index === blockIndex) {
              block.transactions[txIndex].outputs[
                outputIndex
              ].address = address;
              block.hash = calculateHash(
                block.index,
                block.previousHash,
                block.timestamp,
                block.transactions,
                block.nonce
              );
            }
            if (blockIndex < index) {
              const previousHash = node.blockchain[index - 1].hash;
              block.previousHash = previousHash;
              block.hash = calculateHash(
                block.index,
                block.previousHash,
                block.timestamp,
                block.transactions,
                block.nonce
              );
            }
            return block;
          });
        }
        return node;
      });

    case MUTATE_AMOUNT:
      return state.map(node => {
        const { peer, blockIndex, txIndex, outputIndex, amount } = action;
        if (node.peer === peer) {
          node.blockchain = node.blockchain.map((block, index) => {
            if (index === blockIndex) {
              block.transactions[txIndex].outputs[outputIndex].amount = Number(
                amount
              );
              block.hash = calculateHash(
                block.index,
                block.previousHash,
                block.timestamp,
                block.transactions,
                block.nonce
              );
            }
            if (blockIndex < index) {
              const previousHash = node.blockchain[index - 1].hash;
              block.previousHash = previousHash;
              block.hash = calculateHash(
                block.index,
                block.previousHash,
                block.timestamp,
                block.transactions,
                block.nonce
              );
            }
            return block;
          });
        }
        return node;
      });

    case RE_MINE:
      return state.map(node => {
        const { peer, index, nonce, hash, timestamp } = action;
        if (node.peer === peer) {
          node.blockchain = node.blockchain.map((_block, _index) => {
            if (_index === index) {
              _block.nonce = nonce;
              _block.hash = hash;
              _block.timestamp = timestamp;
            }
            if (index < _index) {
              const previousHash = node.blockchain[_index - 1].hash;
              _block.previousHash = previousHash;
              _block.hash = calculateHash(
                _block.index,
                _block.previousHash,
                _block.timestamp,
                _block.transactions,
                _block.nonce
              );
            }
            return _block;
          });
        }
        return node;
      });

    case CONNECT_PEER:
      return state.map(node => {
        if (node.peer === action.fromPeer) {
          const newConnectedPeer = { name: action.toPeer, messages: [] };
          node.connectedPeers = [...node.connectedPeers, newConnectedPeer];
        }
        return node;
      });

    case DISCONNECT_PEER:
      return state.map(node => {
        if (node.peer === action.fromPeer) {
          node.connectedPeers = node.connectedPeers.filter(
            peer => peer.name !== action.toPeer
          );
        }
        return node;
      });

    case REPLACE_CHAIN:
      return state.map(node => {
        if (node.peer === action.peer) {
          node.blockchain = action.chain;
        }
        return node;
      });

    case ADD_HISTORY:
      return state.map(node => {
        if (node.peer === action.fromPeer) {
          node.connectedPeers = node.connectedPeers.map(connectedPeer => {
            if (connectedPeer.name === action.toPeer) {
              connectedPeer.messages = [
                ...connectedPeer.messages,
                action.message
              ];
            }
            return connectedPeer;
          });
        }
        return node;
      });

    default:
      return state;
  }
}

const reducers = combineReducers({
  blockchain
});

export default reducers;

// on the component side

// dispatch => mine the new block
// dispatch => message mined new block, depending on valid chain or not
// check every peer on the blockchain
// if new block can be added => dispatch add new block
// if new block cant be added  => dispatch replace chain
// if new block same length => do nothing
// dispatch => peer's blockchain, new block



// WEBPACK FOOTER //
// ./src/reducers.js