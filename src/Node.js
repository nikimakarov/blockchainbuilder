import React, { Component } from "react";
import {
  incomingConnection,
  disconnectingConnection,
  incomingData,
  sendData,
  removeConnectListener,
  removeDisconnectListener,
  removeDataListener
} from "./PeerToPeer";
import {
  connectPeer,
  disconnectPeer,
  addBlock,
  replaceChain,
  addHistory,
  replaceTransactions,
  addTransaction,
  removeTransaction
} from "./actions.js";
import { isValidNextBlock, isValidChain } from "./Cards/MineCard/util.js";
import { connect } from "react-redux";
import Cards from "./Cards/Cards.js";
import Blockchain from "./Page/Blockchain";
import "./css/Node.css";
import PropTypes from "prop-types";

const messageType = {
  REQUEST_LATEST_BLOCK: 0,
  RECEIVE_LATEST_BLOCK: 1,
  REQUEST_BLOCKCHAIN: 2,
  RECEIVE_BLOCKCHAIN: 3,
  REQUEST_TRANSACTIONS: 4,
  RECEIVE_LATEST_TRANSACTION: 5,
  RECEIVE_TRANSACTIONS: 6,
  RECEIVE_REMOVE_TRANSACTION: 7
};

const {
  REQUEST_LATEST_BLOCK,
  RECEIVE_LATEST_BLOCK,
  REQUEST_BLOCKCHAIN,
  RECEIVE_BLOCKCHAIN,
  REQUEST_TRANSACTIONS,
  RECEIVE_LATEST_TRANSACTION,
  RECEIVE_TRANSACTIONS,
  RECEIVE_REMOVE_TRANSACTION
} = messageType;

class Node extends Component {
  componentDidMount() {
    incomingConnection(this.handleIncomingConnection);
    incomingData(this.handleIncomingData);
    disconnectingConnection(this.handleDisconnectingConnection);
  }

  componentDidUpdate(prevProps, prevState) {
    this.handleBlockchainChange(this.props.blockchain, prevProps.blockchain);
    this.handleTxsChange(this.props.transactions, prevProps.transactions);
  }

  handleBlockchainChange(currentBlockchain, previousBlockchain) {
    if (currentBlockchain.length > previousBlockchain.length) {
      const latestBlock = currentBlockchain[currentBlockchain.length - 1];
      this.broadcast(this.sendLatestBlock(latestBlock));
    }
  }

  handleTxsChange(currentTxs, previousTxs) {
    if (currentTxs.length > previousTxs.length) {
      this.broadcast(this.sendTransactions());
    } else if (currentTxs.length < previousTxs.length) {
      previousTxs.forEach(prevTx => { 
        if (!currentTxs.includes(prevTx)) {
          this.broadcast(this.sendRemoveTransaction(prevTx));
        } 
      });
    }
  }

  componentWillUnmount() {
    removeConnectListener(this.handleIncomingConnection);
    removeDisconnectListener(this.handleDisconnectingConnection);
    removeDataListener(this.handleIncomingData);
  }

  handleIncomingConnection = (fromPeer, toPeer, fromLatestBlock) => {
    const { connectPeer } = this.props;
    if (this.props.toPeer === toPeer) {
      connectPeer(toPeer, fromPeer);
      this.historyHandler(fromPeer, toPeer, "INCOMING_CONNECTION");
      sendData(toPeer, fromPeer, this.getLatestBlock());
      sendData(toPeer, fromPeer, this.getTransactions());
    }
  };

  handleDisconnectingConnection = (fromPeer, toPeer) => {
    if (this.props.toPeer === toPeer) {
      this.props.disconnectPeer(toPeer, fromPeer);
    }
  };

  handleIncomingData = (fromPeer, toPeer, data) => {
    if (this.props.toPeer === toPeer) {
      this.handleMessage(fromPeer, toPeer, data);
    }
  };

  handleMessage = (fromPeer, toPeer, message) => {
    const { blockchain, addTransaction, removeTransaction } = this.props;
    
    switch (message.type) {
      case REQUEST_LATEST_BLOCK:
        const latestBlock = blockchain[blockchain.length - 1];
        this.historyHandler(fromPeer, toPeer, message.type);
        sendData(toPeer, fromPeer, this.sendLatestBlock(latestBlock));
        break;
      case REQUEST_BLOCKCHAIN:
        this.historyHandler(fromPeer, toPeer, message.type);
        sendData(toPeer, fromPeer, this.sendBlockchain());
        break;
      case RECEIVE_BLOCKCHAIN:
        this.historyHandler(fromPeer, toPeer, message.type);
        this.handleReceivedBlockchain(fromPeer, toPeer, message.data);
        break;
      case RECEIVE_LATEST_BLOCK:
        this.historyHandler(fromPeer, toPeer, message.type);
        this.handleReceivedLatestBlock(fromPeer, toPeer, message.data);
        break;
      case REQUEST_TRANSACTIONS:
        this.historyHandler(fromPeer, toPeer, message.type);
        sendData(toPeer, fromPeer, this.sendTransactions());
        break;
      case RECEIVE_TRANSACTIONS:
        this.historyHandler(fromPeer, toPeer, message.type);      
        this.handleReceivedTransactions(fromPeer, toPeer, message.data);
        break;
      case RECEIVE_LATEST_TRANSACTION:
        this.historyHandler(fromPeer, toPeer, message.type);      
        addTransaction(message.data, toPeer);
        break;
      case RECEIVE_REMOVE_TRANSACTION:
        this.historyHandler(fromPeer, toPeer, message.type);
        removeTransaction(message.data, toPeer);
        break;
      default:
        throw new Error("invalid message");
    }
  };

  handleReceivedLatestBlock(fromPeer, toPeer, block) {
    const { blockchain, addBlock } = this.props;
    const latestBlockReceived = JSON.parse(JSON.stringify(block));
    const latestBlockHeld = blockchain[blockchain.length - 1];

    if (latestBlockHeld.hash === latestBlockReceived.previousHash) {
      this.historyHandler(fromPeer, toPeer, "APPEND_BLOCK");
      if (isValidNextBlock(latestBlockReceived, latestBlockHeld)) {
        addBlock(latestBlockReceived, toPeer);
      } else {
        this.historyHandler(fromPeer, toPeer, "INVALID_BLOCK");
      }
    } else if (latestBlockReceived.index <= latestBlockHeld.index) {
      this.historyHandler(fromPeer, toPeer, "DO_NOTHING");
    } else {
      this.historyHandler(fromPeer, toPeer, "ADD_BLOCKCHAIN");
      sendData(toPeer, fromPeer, this.getBlockchain());
    }
  }

  handleReceivedBlockchain(fromPeer, toPeer, blockchainFromPeer) {
    const { blockchain, replaceChain } = this.props;

    const receivedBlockchain = JSON.parse(JSON.stringify(blockchainFromPeer));
    if (receivedBlockchain.length > blockchain.length) {
      this.historyHandler(fromPeer, toPeer, "REPLACE_CHAIN");
      const genesisBlock = blockchain[0];
      if (isValidChain(receivedBlockchain, genesisBlock)) {
        replaceChain(receivedBlockchain, toPeer);
      } else {
        this.historyHandler(fromPeer, toPeer, "INVALID_CHAIN");
      }
    }
  }

  handleReceivedTransactions(fromPeer, toPeer, newTxs) {
    const { transactions, replaceTransactions } = this.props;
    const combinedTx = [...new Set([...transactions, ...newTxs])];

    if (JSON.stringify(combinedTx) !== JSON.stringify(transactions)) {
      replaceTransactions(combinedTx, toPeer);
    } else {
      this.historyHandler(fromPeer, toPeer, "SAME_TRANSACTION");
    }
  }

  broadcast = message => {
    const { connectedPeers, toPeer } = this.props;
    connectedPeers.forEach(connectedPeer => {
      const fromPeer = connectedPeer.name;
      this.historyHandler(fromPeer, toPeer, "BROADCAST");
      sendData(toPeer, fromPeer, message);
    });
  };

  getLatestBlock() {
    return {
      type: REQUEST_LATEST_BLOCK
    };
  }

  sendLatestBlock(block) {
    return {
      type: RECEIVE_LATEST_BLOCK,
      data: block
    };
  }

  getBlockchain() {
    return {
      type: REQUEST_BLOCKCHAIN
    };
  }

  sendBlockchain() {
    return {
      type: RECEIVE_BLOCKCHAIN,
      data: this.props.blockchain
    };
  }

  getTransactions() {
    return {
      type: REQUEST_TRANSACTIONS
    };
  }

  sendTransactions() {
    return {
      type: RECEIVE_TRANSACTIONS,
      data: this.props.transactions
    };
  }

  sendRemoveTransaction = transaction => {
    return {
      type: RECEIVE_REMOVE_TRANSACTION,
      data: transaction
    };
  };

  historyHandler(fromPeer, toPeer, historyType) {
    const { addHistory } = this.props;

    switch (historyType) {
      case REQUEST_LATEST_BLOCK:
        addHistory(toPeer, fromPeer, `${fromPeer} requested for latest block.`);
        addHistory(toPeer, fromPeer, `Sending ${fromPeer} latest block.`);
        break;
      case REQUEST_BLOCKCHAIN:
        addHistory(toPeer, fromPeer, `${fromPeer} requested for blockchain.`);
        addHistory(toPeer, fromPeer, `Sending ${fromPeer} entire blockchain.`);
        break;
      case RECEIVE_BLOCKCHAIN:
        addHistory(toPeer, fromPeer, `${fromPeer} sent over blockchain.`);
        break;
      case RECEIVE_LATEST_BLOCK:
        addHistory(toPeer, fromPeer, `${fromPeer} sent over latest block.`);
        break;
      case REQUEST_TRANSACTIONS:
        addHistory(toPeer, fromPeer, `${fromPeer} requested for transactions.`);
        break;
      case RECEIVE_LATEST_TRANSACTION:
        addHistory(toPeer, fromPeer, `${fromPeer} sent over new transaction.`);
        break;
      case RECEIVE_REMOVE_TRANSACTION:
        addHistory(
          toPeer,
          fromPeer,
          `Remove mined transaction from ${fromPeer}.`
        );
        break;
      case RECEIVE_TRANSACTIONS:
        addHistory(
          toPeer,
          fromPeer,
          `Received unconfirmed transactions from ${fromPeer}.`
        );
        break;
      case "BROADCAST":
        addHistory(toPeer, fromPeer, `Broadcast new block or transaction to ${fromPeer}.`);
        break;
      case "INCOMING_CONNECTION":
        addHistory(toPeer, fromPeer, `${fromPeer} has connected to you.`);
        addHistory(toPeer, fromPeer, `Asking ${fromPeer} for latest block.`);
        addHistory(toPeer, fromPeer, `Asking ${fromPeer} for transactions.`);
        break;
      case "APPEND_BLOCK":
        addHistory(
          toPeer,
          fromPeer,
          `${
            fromPeer
          } block's previous hash is equal to latest block hash. Append received block to blockchain.`
        );
        break;
      case "INVALID_BLOCK":
        addHistory(
          toPeer,
          fromPeer,
          `${fromPeer} block is invalid. Failed to append.`
        );
        break;
      case "DO_NOTHING":
        addHistory(
          toPeer,
          fromPeer,
          `${
            fromPeer
          }'s latest block index is not longer than latest block index. Do nothing.`
        );
        break;
      case "ADD_BLOCKCHAIN":
        addHistory(
          toPeer,
          fromPeer,
          `Asking ${fromPeer} for entire blockchain.`
        );
        break;
      case "REPLACE_CHAIN":
        addHistory(
          toPeer,
          fromPeer,
          `${
            fromPeer
          } blockchain is longer than current chain. Replace blockchain.`
        );
        break;
      case "INVALID_CHAIN":
        addHistory(toPeer, fromPeer, `${fromPeer}'s chain is invalid.`);
        break;
      case "SAME_TRANSACTION":
        addHistory(toPeer, fromPeer, `${fromPeer}'s transaction already exits. Do nothing.`);
        break;
      default:
        throw new Error("invalid message");
    }
  }

  render() {
    return (
      <div className="node">
        <Cards
          peer={this.props.toPeer}
          latestBlock={this.props.blockchain[this.props.blockchain.length - 1]}
          unconfirmedTransactions={this.props.transactions}
          sendLatestBlock={this.sendLatestBlock}
        />
        <Blockchain
          blockchain={this.props.blockchain}
          peer={this.props.toPeer}
        />
      </div>
    );
  }
}

Node.propTypes = {
  connectPeer: PropTypes.func,
  disconnectPeer: PropTypes.func,
  blockchain: PropTypes.array,
  transactions: PropTypes.array,
  connectedPeers: PropTypes.array,
  toPeer: PropTypes.string
};

function mapDispatchToProps(dispatch) {
  return {
    connectPeer: (fromPeer, toPeer) => {
      dispatch(connectPeer(fromPeer, toPeer));
    },
    disconnectPeer: (fromPeer, toPeer) => {
      dispatch(disconnectPeer(fromPeer, toPeer));
    },
    addBlock: (block, peer) => {
      dispatch(addBlock(block, peer));
    },
    addTransaction: (transaction, peer) => {
      dispatch(addTransaction(transaction, peer));
    },
    removeTransaction: (transaction, peer) => {
      dispatch(removeTransaction(transaction, peer));
    },
    replaceChain: (chain, peer) => {
      dispatch(replaceChain(chain, peer));
    },
    replaceTransactions: (transactions, peer) => {
      dispatch(replaceTransactions(transactions, peer));
    },
    addHistory: (fromPeer, toPeer, message) => {
      dispatch(addHistory(fromPeer, toPeer, message));
    }
  };
}

const NodeContainer = connect(null, mapDispatchToProps)(Node);

export default NodeContainer;



// WEBPACK FOOTER //
// ./src/Node.js