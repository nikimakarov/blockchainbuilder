import React, {Component} from "react";
import ReactDom from 'react-dom';
import { Form, Icon, Input, Button, Checkbox, Card, message } from 'antd';
const FormItem = Form.Item;

class NormalLoginForm extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        if (values.userName === 'neo' && values.password === 'neo'){
          var that = this;
          setTimeout(function func() {
            that.props.onStatusUpdate(true);
            message.success(
            `Вход пользователя ${values.userName} успешно выполнен!`
          );
          }, 2000);
        }
        else {
          setTimeout(function func() {
            message.error(
              `Неверные данные авторизации!`
            );
          }, 1500);
        }
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Card className="login-form">
      <Form onSubmit={this.handleSubmit}>
        <FormItem>
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input id="user-in" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input id="user-pass" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
          )}
        </FormItem>
        <FormItem className="no-margin-form">
          {/* {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(
            <Checkbox>Запомнить меня</Checkbox>
          )} */}
          <Button type="primary" htmlType="submit" className="login-form-button">
            Вход
          </Button>
          {/* <div className="login-form-link">
            <a href="">Зарегистрироваться</a>
          </div> */}
        </FormItem>
      </Form>
      </Card>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);

export default WrappedNormalLoginForm;