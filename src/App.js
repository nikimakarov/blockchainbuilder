import React, { Component } from "react";
import PeerTabs from "./PeerTabs/PeerTabs.js";
import Layout from "antd/lib/layout";
import Affix from "antd/lib/affix";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import message from "antd/lib/message";
//import settings from "./Utils/particles.js";
//import Particles from "react-particles-js";
import white from "./img/white.png";
import Social from "./Page/Social.js";
import "./css/App.css";
import NormalLoginForm from "./loginForm.js";
const { Sider, Content } = Layout;

class App extends Component {
  // TODO loggedIn = false
  state = {
    scrollToSteps: false,
    loggedIn: false,
    showTheGraph: false,
    showTheAnalysis: false,
    showLogin: true,
    isPageRank: false
  };

  showGraph = () => {
    console.log('showGraph');
    this.setState({
      showTheGraph: true,
      showLogin: false,
      showTheAnalysis: false,
    });
  };

  showAnalysis = () => {
    console.log('showAnalysis');
    this.setState({
      showTheAnalysis: true,
      showLogin: false,
      showTheGraph: true
    });
  };

  updateLogin = (status) => {
    console.log('logged');
    this.setState({loggedIn: true});
  };

  showBuild = () => {
    this.setState({
      showTheGraph: false,
      showLogin: false
    });
  };

  showLogin = () => {
    if (this.state.loggedIn){
      message.success(
        `Пользователь вышел из системы.`
      );
    }
    this.setState({
      showLogin: true,
      loggedIn: false
    });
  };

  showAll = () => {
    if (window.viz){
      window.viz.renderWithCypher("MATCH p=()-->() RETURN p");
    }
  };
  
  showBlocks = () => {
    if (window.viz){
      window.viz.renderWithCypher("MATCH p=()-[r:chain]->() RETURN p");
    }
  };

  showAddresses = () => {
    if (window.viz){
      window.viz.renderWithCypher("MATCH p=()-[r:locked]->() RETURN p");
    }
  }

  showTransactions = () => {
    if (window.viz){
      window.viz.renderWithCypher("MATCH x=(block :block)<-[:inc]-(tx :tx) RETURN x");
    }
  };

  showBlockTransactions = () => {
    let blockNum = document.getElementById('input-block-transactions').value;
    console.log('blockNum: ', blockNum);
    if (blockNum){
      if (window.viz){
        window.viz.renderWithCypher(`
          MATCH x=(block :block)<-[:inc]-(tx :tx)
          WHERE block.index=${blockNum}
          RETURN x`);
      }
    }
  };

  showRegularTransactions = () => {
    if (window.viz){
      window.viz.renderWithCypher(`
      MATCH x=(inputs)-[:in]->(tx:tx)-[:out]->(outputs)-[:locked]->(outputsaddresses:address)
      WHERE tx.type='regular'
      RETURN x`);
    }
  };

  showBlockRegularTransactions = () => {
    let blockNum = document.getElementById('input-block-transactions').value;
    console.log('blockNum: ', blockNum);
    if (blockNum){
      if (window.viz){
        window.viz.renderWithCypher(`
        MATCH x=(inputs)-[:in]->(tx:tx)-[:out]->(outputs)-[:locked]->(outputsaddresses:address)
        MATCH y=(tx)-[:inc]->(block)
        WHERE (tx.type='regular') and (block.index=${blockNum})
        RETURN x,y`);
      }
    }
  };

  showPageRank = () => {
    if (!this.state.isPageRank && window.viz){
        window.viz.renderWithCypher(`
        MATCH (r1:address)-[:locked]-(:output)-[:out]-(:tx)-[:inc]->(:block)<-[:inc]-(:tx)-[:out]-(:output)-[:locked]-(r2:address)
        WITH r1, r2, COUNT(*) AS count
        CREATE (r2)-[r:TC]->(r1)
        SET r.count = count`);
        function func() {
          window.viz.renderWithCypher(`CALL algo.pageRank('address', 'TC',{write: true, writeProperty:'pagerank'})`);
        }
        function func2() {
          window.viz.renderWithCypher(`MATCH x=(:address)-[n]->(:address) RETURN x`);
        }
        
        setTimeout(func, 1000);
        setTimeout(func2, 2000);

        this.setState({
          isPageRank: true
        });
    }
  };

  showBetweenessCentrality = () => {
    if (window.viz){
      window.viz.renderWithCypher(`CALL algo.betweenness('block','tx', {direction:'out',write:true, writeProperty:'centrality'})`);
      function func2() {
          window.viz.renderWithCypher(`MATCH x=(:block)-[n]->(:block) RETURN x`);
      }
      setTimeout(func2, 1000);
    }
  };

  deleteBetweenessCentrality = () => {
    if (window.viz){
        window.viz.renderWithCypher(`
        MATCH (b:block)
        REMOVE b.centrality
        RETURN b`);
    }
  };

  deletePageRank = () => {
    if (window.viz){
        window.viz.renderWithCypher(`
        MATCH (n)-[r:TC]->()
        DELETE r`);
        window.viz.renderWithCypher(`
        MATCH (a:address)
        REMOVE a.pagerank
        RETURN a`);
        this.setState({
          isPageRank: false
        });
    }
  };

 showShortestPath = () => {
    let addr1 = document.getElementById('addr-1').value;
    let addr2 = document.getElementById('addr-2').value;
    if (addr1 && addr2){
      if (window.viz){
        window.viz.renderWithCypher(`
        MATCH (start :address {address:'${addr1}'}), (end :address {address:'${addr2}'})
        MATCH path=shortestPath( (start)-[:in|:out|:locked*]-(end) )
        RETURN path`);
      }
    }
  };

  showAddressOutputs = () => {
    let addressName = document.getElementById('input-block-address').value;
    console.log('addressName: ', addressName);
    if (addressName){
      if (window.viz){
        window.viz.renderWithCypher(`
        MATCH x=(address :address {address:'${addressName}'})<-[:locked]-(output :output)
        RETURN x`);
      }
    }
  };

  render() {

  return (
      <div>
        <Content
          disabled
          className="mainContent"
          style={{
            paddingLeft: "5vw",
            paddingRight: "5vw",
            paddingTop: "3.5vw",
            paddingBottom: "3.5vw",
            minHeight: "100vh",
          }}>

            {/* <Particles params={settings}
              style={{
                display: "block",
                paddingTop: "7vw",
                position: "fixed",
                top: 0,
                left: 0,
                backgroundColor: "black",
                width: "100%",
                height: "inherit",
                zIndex: "-1000000"
              }} 
            /> */}

            <Affix hidden={!this.state.loggedIn} style={{ left: "5vw", position: "absolute" }}>
              <Button
                type="primary"
                className="button-2 head-button"
                icon="plus-circle-o"
                onClick={this.showBuild}>
                <span style={{ fontWeight: "500" }}>
                  Построение
                </span>
              </Button>
              <Button
                type="primary"
                className="button-2 head-button"
                icon="fork"
                onClick={this.showGraph}>
                <span style={{ fontWeight: "500" }}>
                  Визуализация
                </span>
              </Button>
              <Button
                type="primary"
                className="button-2 head-button"
                icon="fork"
                onClick={this.showAnalysis}>
                <span style={{ fontWeight: "500" }}>
                  Анализ
                </span>
              </Button>
            </Affix>

            <Affix style={{ right: "5vw", position: "absolute" }}>
              <Button
                type="primary"
                className={this.state.loggedIn ? "tour-button-2" : "tour-button"}
                icon="rocket"
                onClick={this.showLogin}>
                <span style={{ fontWeight: "500" }}>
                  {this.state.loggedIn ? "Выйти" : "Вход"}
                </span>
              </Button>
            </Affix>

            <img className="welcome final"
              src={white}
              alt="white.png"
              style={{
                width: "250px",
                marginRight: "auto",
                marginLeft: "auto",
                display: "block"
              }}
            />

            <div id="CreateDiv" hidden={(!this.state.loggedIn && this.state.showLogin) || this.state.showTheGraph}>
              <h1 
                className="p2p section-header">Пиры
               </h1>
              <PeerTabs />
              <Social />
            </div>

            <div className="div-auth" hidden={!this.state.showLogin || this.state.loggedIn}>
              <h1 
                className="p2p section-header centered-h">Авторизация
              </h1>
              <NormalLoginForm loggedIn={this.state.loggedIn} onStatusUpdate={this.updateLogin}/>
            </div>

            <Layout className="marginedLayout" hidden={(!this.state.loggedIn && this.state.showLogin) || !this.state.showTheGraph}>
                <Content className={ !this.state.showTheAnalysis ? "blueSider" : "redSider" }>
                  <div id="viz"></div>
                </Content>

                { !this.state.showTheAnalysis ?


                <Sider className="blueSider">
                  {/* ОБЩИЕ ВЫБОРКИ */}
                  <h3 
                    className="white-head-text button-marg-head">По структуре сети
                  </h3>
                  <Button
                    type="primary"
                    className="button-3 button-marg-head"
                    icon="plus-circle-o"
                    onClick={this.showAll}>
                    <span style={{ fontWeight: "500" }}>
                      Показать все
                    </span>
                  </Button>

                  <Button
                    type="primary"
                    className="button-3 button-marg"
                    icon="plus-circle-o"
                    onClick={this.showBlocks}>
                    <span style={{ fontWeight: "500" }}>
                      Показать все блоки
                    </span>
                  </Button>

                  <Button
                    type="primary"
                    className="button-3 button-marg"
                    icon="plus-circle-o"
                    onClick={this.showAddresses}>
                    <span style={{ fontWeight: "500" }}>
                      Показать все адреса
                    </span>
                  </Button>

                  <Button
                    type="primary"
                    className="button-3 button-marg"
                    icon="plus-circle-o"
                    onClick={this.showTransactions}>
                    <span style={{ fontWeight: "500" }}>
                      Блоки-транзакции
                    </span>
                  </Button>

                  <Button
                    type="primary"
                    className="button-3 button-marg"
                    icon="plus-circle-o"
                    onClick={this.showRegularTransactions}>
                    <span style={{ fontWeight: "500" }}>
                     Регул.транзакции
                    </span>
                  </Button>

                  {/* БЛОК */}
                  <h3 
                    className="white-head-text button-marg-head">По уникальному блоку
                  </h3>
                  
                  <div className="small-margin">
                    <Input id="input-block-transactions" className="button-marg inline-div" placeholder="Введите номер блока" />
                    
                    <Button
                      type="primary"
                      className="button-3 button-marg inline-div"
                      icon="plus-circle-o"
                      onClick={this.showBlockTransactions}>
                      <span style={{ fontWeight: "500" }}>
                      Блок-транзакции
                      </span>
                    </Button>

                    <Button
                      type="primary"
                      className="button-3 button-marg inline-div"
                      icon="plus-circle-o"
                      onClick={this.showBlockRegularTransactions}>
                      <span style={{ fontWeight: "500" }}>
                      Регул.транзакции 
                      </span>
                    </Button>

                  </div>

                  {/* АДРЕС */}
                  <h3 
                    className="white-head-text button-marg-head">По уникальному адресу
                  </h3>

                  <div className="small-margin">
                    <Input id="input-block-address" className="button-marg inline-div" placeholder="Введите адрес" />
                    
                    <Button
                      type="primary"
                      className="button-3 button-marg inline-div"
                      icon="plus-circle-o"
                      onClick={this.showAddressOutputs}>
                      <span style={{ fontWeight: "500" }}>
                      Входные значения
                      </span>
                    </Button>

                  </div>
                  </Sider>
                  :
                  <Sider className="redSider">
                    {/* PAGE RANK */}
                    <h3 
                    className="white-head-text button-marg-head">Рейтинг страницы
                  </h3>
                    <div className="small-margin">
                    <Button
                      type="primary"
                      className="button-6 button-marg inline-div"
                      icon="plus-circle-o"
                      onClick={this.showPageRank}>
                      <span style={{ fontWeight: "500" }}>
                        Применить к адресам
                      </span>
                    </Button>

                    <Button
                      type="primary"
                      className="button-4 inline-div"
                      icon="delete"
                      onClick={this.deletePageRank}>
                      <span style={{ fontWeight: "500" }}>
                        Удалить
                      </span>
                    </Button>
                    </div>

                    {/* CENTRALITY */}
                    <h3 
                    className="white-head-text button-marg-head">Центральность
                    </h3>
                     <div className="small-margin">
                    <Button
                      type="primary"
                      className="button-6 button-marg inline-div"
                      icon="plus-circle-o"
                      onClick={this.showBetweenessCentrality}>
                      <span style={{ fontWeight: "500" }}>
                        Применить к блокам
                      </span>
                    </Button>

                    <Button
                      type="primary"
                      className="button-4 inline-div"
                      icon="delete"
                      onClick={this.deleteBetweenessCentrality}>
                      <span style={{ fontWeight: "500" }}>
                        Удалить
                      </span>
                    </Button>
                    </div>

                    <h3 
                    className="white-head-text button-marg-head">Поиск пути
                    </h3>
                    <div className="small-margin">
                    <Input id="addr-1" className="button-marg inline-div" placeholder="Введите адрес 1" />
                    <Input id="addr-2" className="button-marg inline-div" placeholder="Введите адрес 2" />
                    <Button
                      type="primary"
                      className="button-6 button-marg inline-div"
                      icon="plus-circle-o"
                      onClick={this.showShortestPath}>
                      <span style={{ fontWeight: "500" }}>
                        Найти
                      </span>
                    </Button>
                    </div>



                  </Sider>
                }


            </Layout>

        </Content>
        
      </div>
    );
  }
}

export default App;
