import React from 'react';
import Icon from "antd/lib/icon";
import PropTypes from 'prop-types';

const Arrow = props => {
  if (props.show) {
    return (
      <Icon
        style={{
          display: "block",
          fontSize: "24pt",
          marginBottom: "27px",
          color: "#fff"
        }}
        type="down"
      />
    ); 
  } else {
    return null;
  }
};

Arrow.propTypes = {
  show: PropTypes.bool
};

export default Arrow;



// WEBPACK FOOTER //
// ./src/Arrow.js