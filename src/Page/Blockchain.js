import React from "react";
import Block from "../Block/Block.js";
import Arrow from "./Arrow";

function Blockchain(props) {
  return (
    <div
      style={{
        width: "100%",
        boxShadow: "inset 0 1px 0 0 rgba(0, 0, 0, .1)"
      }}
    >
      <h1
        style={{
          textAlign: "center",
          fontSize: "35pt",
          marginBottom: "33px",
          letterSpacing: "3px",
          position: "relative",
          zIndex: "999",
          marginTop: "50px"
        }}
      >
        Структура блокчейна
      </h1>
      {props.blockchain.map((block, index) => (
        <div key={index}>
          <Block
            currentPeer={props.peer}
            key={index}
            index={block.index}
            transactions={block.transactions}
            timestamp={block.timestamp}
            hash={block.hash}
            nonce={block.nonce}
            previousHash={block.previousHash}
            blockchain={props.blockchain}
          />
          <Arrow show={index !== props.blockchain.length - 1} />
        </div>
      ))}
    </div>
  )
}

export default Blockchain;



// WEBPACK FOOTER //
// ./src/Blockchain.js