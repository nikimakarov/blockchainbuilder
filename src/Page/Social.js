import React from "react";
import Button from "antd/lib/button";
const ButtonGroup = Button.Group;

const Social = () => {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <ButtonGroup>
        <Button icon="user" href="http://nikitamakarov.space"  style={{ color: "black" }}>
          АВТОР
        </Button>
        <Button
          type="primary"
          icon="twitter"
          href="https://twitter.com/nikimakarov"
          target="_blank"
        />
        <Button
          type="primary"
          icon="github"
          href="https://github.com/nikimakarov"
          target="_blank"
        />
        <Button
          type="primary"
          icon="mail"
          href="mailto:nikitjjam@gmail.com"
          target="_blank"
        />
        <Button icon="phone" disabled style={{ color: "black" }}>
          КОНТАКТЫ
        </Button>
      </ButtonGroup>
    </div>
  );
};

export default Social;



// WEBPACK FOOTER //
// ./src/Social.js