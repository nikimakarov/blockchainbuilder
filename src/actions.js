// action types
import { call, put, takeEvery, takeLatest, take } from 'redux-saga/effects';
import storage from 'redux-persist/lib/storage';
import clients from 'restify-clients';
import config from './Utils/configureStore';

// peers
export const ADD_PEER = "ADD_PEER";
export const REMOVE_PEER = "REMOVE_PEER";

// blockchain
export const ADD_BLOCK = "ADD_BLOCK";
export const MUTATE_ADDRESS = "MUTATE_ADDRESS";
export const MUTATE_AMOUNT = "MUTATE_AMOUNT";
export const RE_MINE = "RE_MINE";
export const REPLACE_CHAIN = "REPLACE_CHAIN";

// p2p
export const CONNECT_PEER = "CONNECT_PEER";
export const DISCONNECT_PEER = "DISCONNECT_PEER";
export const ADD_HISTORY = "ADD_HISTORY";

export const REQUEST_LATEST_BLOCK = "REQUEST_LATEST_BLOCK";
export const RECEIVE_LATEST_BLOCK = "RECEIVE_LATEST_BLOCK";
export const REQUEST_BLOCKCHAIN = "REQUEST_BLOCKCHAIN";
export const RECEIVE_BLOCKCHAIN = "RECEIVE_BLOCKCHAIN";
export const REQUEST_TRANSACTIONS = "REQUEST_TRANSACTIONS";
export const RECEIVE_TRANSACTIONS = "RECEIVE_TRANSACTIONS";

// transactions
export const ADD_TRANSACTION = "ADD_TRANSACTION";
export const REMOVE_TRANSACTION = "REMOVE_TRANSACTION";
export const REPLACE_TRANSACTIONS = "REPLACE_TRANSACTIONS";

// action creators
export function addPeer() {
  return {
    type: ADD_PEER
  };
}

export function removePeer(peer) {
  return {
    type: REMOVE_PEER,
    peer
  };
}

export function addBlock(block, peer) {
  console.log(block, peer);
  return {
    type: ADD_BLOCK,
    block,
    peer
  };
}

export function mutateAddress(
  peer,
  blockIndex,
  txIndex,
  outputIndex,
  address
) {
  return {
    type: MUTATE_ADDRESS,
    peer,
    blockIndex,
    txIndex,
    outputIndex,
    address
  };
}

export function mutateAmount(
  peer,
  blockIndex,
  txIndex,
  outputIndex,
  amount
) {
  return {
    type: MUTATE_AMOUNT,
    peer,
    blockIndex,
    txIndex,
    outputIndex,
    amount
  };
}

export function reMine(index, peer, nonce, hash, timestamp) {
  return {
    type: RE_MINE,
    index,
    nonce,
    hash,
    timestamp,
    peer
  };
}

export function addTransaction(transaction, peer) {
  return {
    type: ADD_TRANSACTION,
    transaction,
    peer
  }
}

export function removeTransaction(transaction, peer) {
  return {
    type: REMOVE_TRANSACTION,
    transaction,
    peer
  }
}

export function replaceTransactions(transactions, peer) {
  return {
    type: REPLACE_TRANSACTIONS,
    transactions,
    peer
  }
}

export function connectPeer(fromPeer, toPeer) {
  return {
    type: CONNECT_PEER,
    fromPeer,
    toPeer
  }
}

export function disconnectPeer(fromPeer, toPeer) {
  return {
    type: DISCONNECT_PEER,
    fromPeer,
    toPeer
  }
}

export function replaceChain(chain, peer) {
  return {
    type: REPLACE_CHAIN,
    chain,
    peer
  }
}

export function addHistory(fromPeer, toPeer, message) {
  return {
    type: ADD_HISTORY,
    fromPeer,
    toPeer,
    message
  }
}

export function* addFetchBlock() {
  yield takeLatest('ADD_BLOCK', fetchBlockAsync);
}

export function* fetchBlockAsync() {
  try {
    yield call(() => {
      console.log('store:',config.store.getState());
      var block = config.store.getState().blockchain;
      return fetch('http://localhost:3010/', {
        method: 'POST',
        body: JSON.stringify(block)
      }).then(res => console.log(res))
      }
    );
  } catch (error) {
  }
}

// WEBPACK FOOTER //
// ./src/actions.js