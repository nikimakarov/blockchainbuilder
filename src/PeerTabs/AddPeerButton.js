import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import { addPeer } from "../actions.js";
import { connect } from "react-redux";


const AddPeerButton = props => {
  return (
    <Button
      className="white-add-peer"
      type="primary"
      onClick={() => {
        props.addPeer();
      }}
      icon="user-add"
    >
      Добавить пиры
    </Button>
  );
};

AddPeerButton.propTypes = {
  addPeer: PropTypes.func,
  setNewPeerAsActiveTab: PropTypes.func
};

function mapDispatchToProps(dispatch) {
  return {
    addPeer: () => {
      dispatch(addPeer());
    }
  };
}

const AddPeerButtonContainer = connect(null, mapDispatchToProps)(AddPeerButton);

export default AddPeerButtonContainer;


// WEBPACK FOOTER //
// ./src/PeerTabs/AddPeerButton.js