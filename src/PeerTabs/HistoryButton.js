import PropTypes from "prop-types";
import Button from "antd/lib/button";
import React from "react";

const HistoryButton = props => {
  return (
    <Button
      style={{ marginLeft: "10px"}}
      size="small"
      onClick={(e) => {
        e.stopPropagation();
        props.selectOtherPeer();
        props.showModal();
      }}
      icon="message"
      shape="circle"
    >
    </Button>
  );
}

HistoryButton.propTypes = {
  showModal: PropTypes.func
};

export default HistoryButton;


// WEBPACK FOOTER //
// ./src/PeerTabs/HistoryButton.js