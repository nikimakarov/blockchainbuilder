import React from "react";
import Avatar from "antd/lib/avatar";
import PropTypes from "prop-types";
import "../css/PeerTabs/PeerAvatar.css";
import "../css/PeerTabs/TabContent.css";
import Badge from "antd/lib/badge";

const PeerAvatar = props => {
  const { isConnected, isCurrentPeer, currentPeer, connectedPeers } = props;

  return (
    <Badge
      // count={msgLength(connectedPeers, currentPeer, isCurrentPeer, isConnected)}
    >
      <Avatar
        className={isConnected ? "green dim" : isCurrentPeer ? "blue" : "red dim"}
        size="large"
        icon="user"
      />
    </Badge>
  );
};

function msgLength(connectedPeers, currentPeer, isCurrentPeer, isConnected) {
  if (!isCurrentPeer && isConnected) {
    return connectedPeers.find(peer => peer.name === currentPeer).messages
      .length;
  } else {
    return 0;
  }
}

PeerAvatar.propTypes = {
  isConnected: PropTypes.bool,
  isCurrentPeer: PropTypes.bool,
  currentPeer: PropTypes.string
};

export default PeerAvatar;



// WEBPACK FOOTER //
// ./src/PeerTabs/PeerAvatar.js