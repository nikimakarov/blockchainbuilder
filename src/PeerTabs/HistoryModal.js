import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Modal from "antd/lib/modal";
import Timeline from "antd/lib/timeline";
import Alert from "antd/lib/alert";
import Icon from "antd/lib/icon";

const HistoryModal = props => {
  const {
    blockchain,
    currentPeer,
    otherPeer,
    isModalVisible,
    handleOk,
    handleCancel
  } = props;
  const messages = getPeerMessages(currentPeer, otherPeer, blockchain);

  return (
    <Modal
      title={
        <span>
          {otherPeer}
          <Icon
            style={{ marginRight: "5px", marginLeft: "5px" }}
            type="customer-service"
          />
          {currentPeer}
        </span>
      }
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="OK"
      cancelText="Cancel"
      zIndex={9999}
      bodyStyle={{
        paddingBottom: "0px",
        paddingLeft: "27px",
        paddingRight: "27px",
        paddingTop: "27px"
      }}
    >
      <Timeline>
        {messages.length !== 0 ? messages.map((message, index) => {
          const timelineIcon = getTimelineIcon(message);
          return (
            <Timeline.Item
              key={index}
              dot={
                <Icon
                  type={timelineIcon.icon}
                  style={{ color: timelineIcon.color, fontSize: 16 }}
                />
              }
            >
              {message}
            </Timeline.Item>
          );
        }) : 
        <Alert
          message={`No messages between ${currentPeer} and ${otherPeer}.`}
          type="error"
          style={{ marginBottom: "27px" }}
        />
        }
      </Timeline>
    </Modal>
  );

};

function getPeerMessages(currentPeer, otherPeer, blockchain) {
  const currentNode = blockchain.find(node => node.peer === currentPeer);
  const connectedPeer = currentNode.connectedPeers.find(
    connectedPeer => connectedPeer.name === otherPeer
  );

  if (connectedPeer !== undefined) {
    return connectedPeer.messages;
  } else {
    return [];
  }
}

function getTimelineIcon(message) {
  const blue = "#108ee9";
  const green = "#00a854";
  const red = "#f04134";

  if (
    message.includes("Connected to peer") ||
    message.includes("has connected to you")
  ) {
    return { icon: "team", color: green };
  } else if (message.includes("Asking")) {
    return { icon: "cloud-download", color: blue };
  } else if (message.includes("requested")) {
    return { icon: "cloud-upload", color: blue };
  } else if (message.includes("Sending")) {
    return { icon: "upload", color: blue };
  } else if (message.includes("sent over")) {
    return { icon: "download", color: blue };
  } else if (message.includes("Do nothing.")) {
    return { icon: "safety", color: green };
  } else if (message.includes("possibly behind")) {
    return { icon: "hourglass", color: "GoldenRod" };
  } else if (message.includes("Get entire blockchain from peer.")) {
    return { icon: "database", color: "GoldenRod" };
  } else if (message.includes("Append received block")) {
    return { icon: "copy", color: green };
  } else if (message.includes("blockchain is longer")) {
    return { icon: "switcher", color: green };
  } else if (message.includes("Received unknown message")) {
    return { icon: "question", color: red };
  } else if (message.includes("blockchain is shorter")) {
    return { icon: "close", color: red };
  } else if (message.includes("invalid.")) {
    return { icon: "close", color: red };
  } else if (message.includes("Broadcast")) {
    return { icon: "notification", color: blue };
  }
  return { icon: "question", color: blue };
}

function mapStateToProps(state) {
  return {
    blockchain: state.blockchain
  };
}

HistoryModal.propTypes = {
  blockchain: PropTypes.array,
  currentPeer: PropTypes.string,
  otherPeer: PropTypes.string
};

const HistoryModalContainer = connect(mapStateToProps, null)(HistoryModal);

export default HistoryModalContainer;



// WEBPACK FOOTER //
// ./src/PeerTabs/HistoryModal.js