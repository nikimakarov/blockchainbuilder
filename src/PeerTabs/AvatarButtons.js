import React, { Component } from "react";
import ConnectButton from "./ConnectButton.js";
import HistoryButton from "./HistoryButton.js";
import PropTypes from "prop-types";

class AvatarButtons extends Component {
  render() {
    const {
      isCurrentPeer,
      currentPeer,
      otherPeer,
      isConnected,
      showModal,
      selectOtherPeer
    } = this.props;

    if (!isCurrentPeer) {
      return (
        <div
          style={{ display: "flex", justifyContent: "center", opacity: "1" }}
        >
          <ConnectButton
            currentPeer={currentPeer}
            otherPeer={otherPeer}
            isConnected={isConnected}
          />
          {/* {isConnected ? (
            <HistoryButton 
              showModal={showModal}
              selectOtherPeer={() => selectOtherPeer(otherPeer)}
            />
          ) : null} */}
        </div>
      );
    } else {
      return null;
    }
  }
}

AvatarButtons.propTypes = {
  isCurrentPeer: PropTypes.bool,
  currentPeer: PropTypes.string,
  otherPeer: PropTypes.string,
  isConnected: PropTypes.bool
};

export default AvatarButtons;



// WEBPACK FOOTER //
// ./src/PeerTabs/AvatarButtons.js