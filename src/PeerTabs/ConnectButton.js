import React from "react";
import PropTypes from "prop-types";
import Button from "antd/lib/button";
import { connectToPeer, disconnectFromPeer, sendData } from "../PeerToPeer";
import { addHistory, connectPeer, disconnectPeer } from "../actions.js";
import { connect } from "react-redux";
import message from "antd/lib/message";
import "../css/PeerTabs/ConnectButton.css";

const ConnectButton = props => {
  const { currentPeer, otherPeer, isConnected } = props;

  return (
    <Button
      className={`${isConnected ? "peer-disconnect" : "peer-connect"}`}
      size="small"
      shape="circle"
      icon={`${isConnected ? "usergroup-delete" : "usergroup-add"}`}
      onClick={e => {
        e.stopPropagation();
        const { disconnectPeer, connectPeer, addHistory } = props;
        if (isConnected) {
          disconnectFromPeer(currentPeer, otherPeer);
          disconnectPeer(currentPeer, otherPeer);
          disconnectMsg(otherPeer);
        } else {
          connectMsg(otherPeer);
          connectPeer(currentPeer, otherPeer);
          addHistoryBulk(addHistory, currentPeer, otherPeer);
          connectToPeer(currentPeer, otherPeer);
          sendData(currentPeer, otherPeer, { type: 0 });
        }
      }}
    >
    </Button>
  );
};

function addHistoryBulk(fn, fromPeer, toPeer) {
  fn(fromPeer, toPeer, ` Соединение с пиром ${toPeer}.`);
  fn(fromPeer, toPeer, `Запрос последнего блока у ${toPeer}.`);
}

function disconnectMsg(peer) {
  message.success(
    `Соединение с пиром ${
      peer
    } приостановлено.`
  );
}

function connectMsg(peer) {
  message.success(
    `Соединение с пиром ${
      peer
    }.`
  );
}

function mapDispatchToProps(dispatch) {
  return {
    addHistory: (fromPeer, toPeer, message) => {
      dispatch(addHistory(fromPeer, toPeer, message));
    },
    connectPeer: (fromPeer, toPeer) => {
      dispatch(connectPeer(fromPeer, toPeer));
    },
    disconnectPeer: (fromPeer, toPeer) => {
      dispatch(disconnectPeer(fromPeer, toPeer));
    }
  };
}

const ConnectButtonContainer = connect(null, mapDispatchToProps)(ConnectButton);

ConnectButton.propTypes = {
  currentPeer: PropTypes.string,
  otherPeer: PropTypes.string,
  connectedPeers: PropTypes.array,
  addHistory: PropTypes.func,
  connectPeer: PropTypes.func,
  disconnectPeer: PropTypes.func
};
export default ConnectButtonContainer;



// WEBPACK FOOTER //
// ./src/PeerTabs/ConnectButton.js