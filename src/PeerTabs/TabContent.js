import React, { Component } from "react";
import PropTypes from "prop-types";
import PeerAvatar from "./PeerAvatar";
import AvatarButtons from "./AvatarButtons.js";
import "../css/PeerTabs/TabContent.css";

class TabContent extends Component {
  state = {
    isConnected: isConnected(this.props.currentPeer, this.props.connectedPeers)
  };

  componentWillReceiveProps(nextProps) {
    if (
      this.props.currentPeer !== nextProps.currentPeer ||
      JSON.stringify(this.props.connectedPeers) !==
        JSON.stringify(nextProps.connectedPeers)
    ) {
      this.setState({
        isConnected: isConnected(
          nextProps.currentPeer,
          nextProps.connectedPeers
        )
      });
    }
  }

  render() {
    const {
      otherPeer,
      currentPeer,
      connectedPeers,
      showModal,
      selectOtherPeer
    } = this.props;
    const isCurrentPeer = currentPeer === otherPeer;
    return (
      <div style={{ display: "inline-grid", marginTop: "15px", marginBottom: "15px" }}>
        <PeerAvatar
          isConnected={this.state.isConnected}
          isCurrentPeer={isCurrentPeer}
          currentPeer={currentPeer}
          connectedPeers={connectedPeers}
        />
        <div
          style={{ textAlign: "center", marginTop: "5px" }}
          className={!isCurrentPeer ? "dim" : ""}
        >
          {otherPeer}
        </div>
        <AvatarButtons
          isCurrentPeer={isCurrentPeer}
          currentPeer={currentPeer}
          otherPeer={otherPeer}
          isConnected={this.state.isConnected}
          showModal={showModal}
          selectOtherPeer={selectOtherPeer}
        />
      </div>
    );
  }
}

function isConnected(peer, connectedPeers) {
  return connectedPeers.some(connectedPeer => connectedPeer.name === peer);
}

TabContent.propTypes = {
  otherPeer: PropTypes.string,
  currentPeer: PropTypes.string,
  peer: PropTypes.string,
  connectedPeers: PropTypes.array
};

export default TabContent;



// WEBPACK FOOTER //
// ./src/PeerTabs/TabContent.js