import React, { Component } from "react";
import Tabs from "antd/lib/tabs";
import { removePeer } from "../actions.js";
import { connect } from "react-redux";
import AddPeerButton from "./AddPeerButton.js";
import TabContent from "./TabContent.js";
import Node from "../Node.js";
import PropTypes from "prop-types";
import HistoryModal from "./HistoryModal.js";
const TabPane = Tabs.TabPane;

class PeerTabs extends Component {
  state = { activeTab: "0", isModalVisible: false, otherPeer: "" }

  showModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    });
  };

  selectOtherPeer = (otherPeer) => {
    this.setState({ otherPeer })
  }

  onChange = activeTab => {
    this.setState({ activeTab });
  };

  onEdit = (targetTab, action) => {
    this[action](targetTab);
  };

  remove = targetTab => {
    this.props.removePeer(this.props.blockchain[targetTab].peer);

    if(this.state.activeTab === 0 && this.targetTab === 0) {
      this.setState({ activeTab: "0" });
    }

    if (this.state.activeTab >= targetTab && this.state.activeTab !== "0") {
      this.setState({ activeTab: `${Number(this.state.activeTab) - 1}` });
    }
  };

  render() {
    const { blockchain } = this.props;

    return (
      <Tabs
        hideAdd={true}
        type="editable-card"
        activeKey={this.state.activeTab}
        onChange={this.onChange}
        onEdit={this.onEdit}
        tabBarExtraContent={
          <AddPeerButton />
        }
        animated={true}
      >
        {blockchain.map((node, index) => {
          const currentPeer = blockchain[Number(this.state.activeTab)].peer

          return (
            <TabPane
              key={index}
              tab={
                <TabContent
                  otherPeer={node.peer}
                  currentPeer={currentPeer}
                  connectedPeers={node.connectedPeers}
                  showModal={this.showModal}
                  selectOtherPeer={this.selectOtherPeer}
                />
              }
              forceRender={true}
            >
              <Node
                toPeer={node.peer}
                transactions={node.transactions}
                connectedPeers={node.connectedPeers}
                blockchain={node.blockchain}
              />
              <HistoryModal
                currentPeer={currentPeer}
                otherPeer={this.state.otherPeer}
                handleOk={this.showModal}
                handleCancel={this.showModal}
                isModalVisible={this.state.isModalVisible}
              />
            </TabPane>
          );
        })}
      </Tabs>
    );
  }
}

PeerTabs.proptypes = {
  blockchain: PropTypes.array,
  removePeer: PropTypes.func
};

function mapStateToProps(state) {
  return {
    blockchain: state.blockchain
  };
}

function mapDispatchToProps(dispatch) {
  return {
    removePeer: index => {
      dispatch(removePeer(index));
    }
  };
}

const PeerTabsContainer = connect(mapStateToProps, mapDispatchToProps)(
  PeerTabs
);

export default PeerTabsContainer;



// WEBPACK FOOTER //
// ./src/PeerTabs/PeerTabs.js