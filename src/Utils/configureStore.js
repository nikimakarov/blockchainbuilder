import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducers from '../reducers.js';
import {getData} from './databaseConnection.js';
import {addFetchBlock} from '../actions.js';
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
    key: 'root',
    storage
};
const persistedReducer = persistReducer(persistConfig, reducers);

let store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(addFetchBlock);
let persistor = persistStore(store);
let config = {store, persistor};
getData();

export default config;
 