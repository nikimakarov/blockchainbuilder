import names from './names.js';

// peer functions
export function generateName(existingNames = []) {
  const filteredNames = names.filter(name => !existingNames.includes(name));
  return filteredNames[Math.floor(Math.random() * filteredNames.length)];
}
