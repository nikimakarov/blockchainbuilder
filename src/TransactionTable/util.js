import TransactionEdit from "./TransactionEdit.js";
import Icon from "antd/lib/icon";
import Tag from "antd/lib/tag";
import { getAllTransactionInputs } from "../Cards/util.js";
import React from "react";

export function getColumns(data, onEdit, editDone, renderColumns) {
  return [
    {
      title: (
        <span>
          <Icon type="up-circle-o" /> От кого
        </span>
      ),
      dataIndex: "from",
      render: (text, record, index) => renderColumns(data, index, "from", text)
    },
    {
      title: (
        <span>
          <Icon type="down-circle-o" /> Кому
        </span>
      ),
      dataIndex: "to",
      render: (text, record, index) => renderColumns(data, index, "to", text)
    },
    {
      title: (
        <span>
          <Icon type="pay-circle-o" /> Величина
        </span>
      ),
      dataIndex: "amount",
      render: (text, record, index) =>
        renderColumns(data, index, "amount", text)
    },
    {
      title: (
        <span>
          <Icon type="info-circle-o" /> Статус
        </span>
      ),
      dataIndex: "status",
      render: (text, record, index) =>
        renderColumns(data, index, "status", text)
    }, // spent or unspent
    {
      title: (
        <span>
          <Icon type="exclamation-circle-o" /> Атака
        </span>
      ),
      dataIndex: "hack",
      key: "hack",
      render: (text, record, index) => (
        <TransactionEdit
          index={index}
          isEditing={data[index].to.isEditing}
          onEdit={onEdit}
          editDone={editDone}
        />
      )
    }
  ];
}

export function cleanData(data, currentPeer, blockchain, blockIndex) {
  const outputs = [];
  data.forEach((transaction, transactionIndex) => {
    transaction.outputs.forEach((output, outputIndex) => {
      outputs.push({
        key: `${transactionIndex} ${outputIndex}`,
        from: {
          value: fromInput(transaction.type, transaction.inputs, output.address)
        },
        to: {
          value: output.address,
          isEditing: false
        },
        status: {
          value: isOutputSpent(
            {
              transaction: [blockIndex, transactionIndex, outputIndex],
              index: transactionIndex,
              amount: output.amount,
              address: output.address
            },
            output.address,
            blockchain
          )
        },
        amount: { value: output.amount, isEditing: false },
        outputIndex: outputIndex
      });
    });
  });

  return outputs;
}

function isOutputSpent(output, currentPeer, blockchain) {
  const allTransactionInputs = getAllTransactionInputs(currentPeer, blockchain);
  console.log(output)
  const isSpent = allTransactionInputs.some(
    input => JSON.stringify(input) === JSON.stringify(output)
  );
  if (isSpent) {
    return <Tag color="red">ИСПОЛЬЗОВАННЫЕ</Tag>;
  } else {
    return <Tag color="green">НЕИСПОЛЬЗОВАННЫЕ</Tag>;
  }
}

function fromInput(type, inputs, toAddress) {
  if (type === "reward") {
    return (
      <span>
        <Icon type="trophy" /> ВОЗНАГРАЖДЕНИЕ
      </span>
    );
  } else if (type === "fee") {
    return (
      <span>
        <Icon type="red-envelope" /> КОМИССИОННЫЕ
      </span>
    );
  } else if (type === "regular") {
    const fromAddress = inputs[0].address
    if (fromAddress === toAddress) {
      return (
        <span>
          <Icon type="retweet" /> ВОЗВРАТ
        </span>
      )
    } else {
      return <span>{fromAddress}</span>;
    }
  } else {
    throw new Error(`invalid type ${type}`);
  }
}



// WEBPACK FOOTER //
// ./src/TransactionsTable/util.js