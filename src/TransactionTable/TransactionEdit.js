import React, { Component } from 'react';

class TransactionEdit extends Component {
  render() {
    return (
      <div>
      {
        this.props.isEditing ?
          <span>
            <a onClick={() => this.props.editDone(this.props.index, 'save')}>Save</a>
            <a onClick={() => this.props.editDone(this.props.index, 'cancel')}> Cancel</a>
          </span>
          :
          <span>
            <a style={{ color: "#f5222d" }} onClick={() => this.props.onEdit(this.props.index)}>Изменить</a>
          </span>
      }
    </div>
    );
  }
}

export default TransactionEdit;


// WEBPACK FOOTER //
// ./src/TransactionsTable/TransactionEdit.js