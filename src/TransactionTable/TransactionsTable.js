import React, { Component } from "react";
import Table from "antd/lib/table";
import EditableCell from "./EditableCell";
import { connect } from "react-redux";
import { mutateAddress, mutateAmount } from "../actions.js";
import { getColumns, cleanData } from "./util.js";

class TransactionsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: cleanData(
        this.props.data,
        this.props.currentPeer,
        this.props.blockchain,
        this.props.blockIndex
      )
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      data: cleanData(
        nextProps.data,
        nextProps.currentPeer,
        nextProps.blockchain,
        nextProps.blockIndex
      )
    });
  }

  onEdit = index => {
    const data = this.state.data;
    Object.keys(data[index]).forEach(item => {
      if (
        data[index][item] &&
        typeof data[index][item].isEditing !== "undefined"
      ) {
        data[index][item].isEditing = true;
      }
    });
    this.setState({ data });
  };

  editDone = (index, type) => {
    const data = this.state.data;
    Object.keys(data[index]).forEach(item => {
      if (
        data[index][item] &&
        typeof data[index][item].isEditing !== "undefined"
      ) {
        data[index][item].isEditing = false;
        data[index][item].editResult = type;
      }
    });

    this.setState({ data }, () => {
      Object.keys(data[index]).forEach(item => {
        if (
          data[index][item] &&
          typeof data[index][item].isEditing !== "undefined"
        ) {
          delete data[index][item].editResult;
        }
      });
    });
  };

  handleChange = (key, index, value, outputIndex) => {
    const { mutateAddress, mutateAmount, currentPeer, blockIndex } = this.props;
    if (key === "to") {
      mutateAddress(currentPeer, blockIndex, index, outputIndex, value);
    } else if (key === "amount") {
      mutateAmount(currentPeer, blockIndex, index, outputIndex, value);
    }
  };

  renderColumns = (data, index, key, text) => {
    const { outputIndex } = data[index];
    const { isEditing, editResult } = data[index][key];
    if (typeof isEditing === "undefined") {
      return text;
    }
    return (
      <EditableCell
        currentPeer={this.props.currentPeer}
        col={key}
        isEditing={isEditing}
        value={text}
        onChange={value => this.handleChange(key, index, value, outputIndex)}
        editResult={editResult}
      />
    );
  };

  render() {
    return (
      <div className={this.props.blockIndex === 1 ? "table-1 table-2" : this.props.blockIndex === 2 ? "table-3" : ""}>
          <Table
            pagination={false}
            columns={getColumns(
              this.state.data,
              this.onEdit,
              this.editDone,
              this.renderColumns
            )}
            dataSource={dataForTable(this.state.data)}
          />
      </div>
    );
  }
}

function dataForTable(data) {
  console.log(data);
  const dataSource = data.map(item => {
    console.log(item);
    const obj = {};
    Object.keys(item).forEach(key => {
      obj[key] = key === "key" ? item[key] : item[key].value;
    });
    return obj;
  });
  return dataSource;
}

function mapDispatchToProps(dispatch) {
  return {
    mutateAddress: (
      peer,
      blockIndex,
      transactionIndex,
      outputIndex,
      address
    ) => {
      dispatch(
        mutateAddress(peer, blockIndex, transactionIndex, outputIndex, address)
      );
    },
    mutateAmount: (peer, blockIndex, transactionIndex, outputIndex, amount) => {
      dispatch(
        mutateAmount(peer, blockIndex, transactionIndex, outputIndex, amount)
      );
    }
  };
}

const TransactionsTableContainer = connect(null, mapDispatchToProps)(
  TransactionsTable
);

export default TransactionsTableContainer;



// WEBPACK FOOTER //
// ./src/TransactionsTable/TransactionsTable.js