import React, { Component } from "react";
import Input from "antd/lib/input";

class EditableCell extends Component {
  state = {
    value: this.props.value,
    isEditing: this.props.isEditing || false
  };
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.isEditing !== this.state.isEditing) {
      this.setState({ isEditing: nextProps.isEditing });
      if (nextProps.isEditing) {
        this.cacheValue = this.state.value;
      }
    }
    if (
      nextProps.editResult &&
      nextProps.editResult !== this.props.editResult
    ) {
      if (
        nextProps.editResult === "save" &&
        this.cacheValue !== this.state.value
      ) {
        console.log("save executed! use new value", this.state.value);
        this.props.onChange(this.state.value);
      } else if (
        nextProps.editResult === "cancel" &&
        this.state.value !== this.cacheValue
      ) {
        console.log("cancel executed! use cache value", this.cacheValue);
        this.setState({ value: this.cacheValue });
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.isEditing !== this.state.isEditing ||
      nextState.value !== this.state.value
    );
  }
  handleChange(e) {
    const value = e.target.value;
    this.setState({ value });
  }
  render() {
    const { value, isEditing } = this.state;

    if (isEditing) {
      return (
        <Input
          style={{ maxWidth: "66px" }}
          size="small"
          type={this.props.col === "amount" ? "number" : "text"}
          value={value}
          onChange={e => this.handleChange(e)}
        />
      );
    } else {
      return (
        <div>
          {value}
        </div>
      );
    }
  }
}

export default EditableCell;



// WEBPACK FOOTER //
// ./src/TransactionsTable/EditableCell.js