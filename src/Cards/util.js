export function getBalance(peer, blockchains) {
  const blockchain = findPeerBlockchain(peer, blockchains);
  const unspentTxOutputs = getUnspentTransactionOutputs(peer, blockchain);
  const unspentTotal = getTotal(unspentTxOutputs);
  return unspentTotal;
}

export function getTotal(array) {
  return array.reduce((total, item) => total + item.amount, 0);
}

function findPeerBlockchain(peer, blockchains) {
  return blockchains.find(blockchain => blockchain.peer === peer).blockchain;
}

export function getUnspentTransactionOutputs(peer, blockchain) {
  let inputs = getAllTransactionInputs(peer, blockchain);
  let outputs = getAllTransactionOutputs(peer, blockchain);

  const unspentTransactionOutputs = filterSpentOutputs(outputs, inputs);
  return unspentTransactionOutputs;
}

export function filterSpentOutputs(outputs, inputs) {
  return outputs.filter(
    output =>
      !inputs.some(input => JSON.stringify(input) === JSON.stringify(output))
  );
}

export function getAllTransactionInputs(peer, blockchain) {
  let inputs = [];
  blockchain.forEach(block => {
    block.transactions.forEach((transaction) => {
      transaction.inputs.forEach(input => {
        if (input.address === peer) {
          inputs.push({
            transaction: input.transaction,
            index: input.index,
            amount: input.amount,
            address: input.address
          });
        }
      });
    });
  });
  return inputs;
}

function getAllTransactionOutputs(peer, blockchain) {
  let outputs = [];
  blockchain.forEach((block, blockIndex) => {
    block.transactions.forEach((transaction, transactionIndex) => {
      transaction.outputs.forEach((output, outputIndex) => {
        if (output.address === peer) {
          outputs.push({
            transaction: [blockIndex, transactionIndex, outputIndex],
            index: transactionIndex,
            amount: output.amount,
            address: output.address
          });
        }
      });
    });
  });
  return outputs;
}



// WEBPACK FOOTER //
// ./src/Cards/util.js