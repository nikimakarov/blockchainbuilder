import React from "react";
import Card from "antd/lib/card";
import { connect } from "react-redux";
import { getBalance } from "../util.js";
import PropTypes from "prop-types";
import "../../css/Cards/BalanceCard/BalanceCard.css";

const BalanceCard = props => {
  return (
    <Card
      className={`${props.peer === "Satoshi"
        ? "balance-1"
        : null} balance-card money-shadow`}
    >
      <div className="balance-number">
        {getBalance(props.peer, props.blockchain).toLocaleString()}
      </div>
      <div className="balance-text">Баланс</div>
    </Card>
  );
};

BalanceCard.propTypes = {
  peer: PropTypes.string,
  blockchain: PropTypes.array
};

function mapStateToProps(state) {
  return {
    blockchain: state.blockchain
  };
}

const BalanceContainer = connect(mapStateToProps, null)(BalanceCard);

export default BalanceContainer;



// WEBPACK FOOTER //
// ./src/Cards/BalanceCard/BalanceCard.js