import React from "react";
import Card from "antd/lib/card";
import Select from "antd/lib/select";
import InputNumber from "antd/lib/input-number";
import Input from "antd/lib/input";
import Form from "antd/lib/form";
import message from "antd/lib/message";
import { connect } from "react-redux";
import PayButton from "./PayButton.js";
import {
  createTransaction,
  getConfirmedUnspentTxOutputs,
  getInputAddressFromOutput,
} from "./util.js";
import { getBalance, getTotal } from "../util.js";
import { addTransaction } from "../../actions.js";
import  "../../css/Cards/PayCard/PayCard.css";
const InputGroup = Input.Group;
const FormItem = Form.Item;
const Option = Select.Option;

class Pay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      to: null,
      amount: 0,
      fee: 0,
      inputs: [],
      validateStatusAmountAndFee: null,
      errorMsgAmountFee: "Сумма: 0",
      validateStatusInputs: null,
      errorMsgInputs: "Сумма: 0"
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.blockchain !== nextProps.blockchain) {
      this.handleAmountChange(this.state.amount);
      this.handleFeeChange(this.state.fee);
    }
  }

  handleSubmit = (e, blockchain) => {
    e.preventDefault();

    const {
      to,
      amount,
      fee,
      inputs,
      validateStatusAmountAndFee,
      validateStatusInputs
    } = this.state;
    const { peer, form, addTransaction } = this.props;
    form.validateFields((err, values) => {
      if (
        !err &&
        validateStatusAmountAndFee === "success" &&
        validateStatusInputs === "success" && this.state.to !== null
      ) {
        let transaction = createTransaction(
          peer,
          to,
          amount,
          fee,
          blockchain,
          inputs
        );
        addTransaction(transaction, peer);
        message.success(
          `Успешно создана транзакция от ${peer} к ${to} с суммой: ${amount} и комиссией: ${fee}`
        );
        this.setState({
          to: null,
          amount: 0,
          fee: 0,
          inputs: [],
          validateStatusAmountAndFee: null,
          errorMsgAmountFee: null,
          validateStatusInputs: null,
          errorMsgInputs: null
        });
        form.resetFields();
      } else {
        // didnt submit properly
        if (validateStatusInputs !== "success") {
          message.error(`Введите валидные значения!`);
        }
        
        if (validateStatusAmountAndFee !== "success") {
          message.error(`Введите валидную сумму и комиссию!`);
        }

        if(this.state.to === null) {
          message.error(`Выберите пир!`);
        }
      }
    });
  };

  handleAmountChange = amount => {
    this.setState(this.validateBalance(amount, this.state.fee), () =>
      this.setState(this.validateInputs(this.state.inputs))
    );
  };

  handleFeeChange = fee => {
    this.setState(this.validateBalance(this.state.amount, fee), () =>
      this.setState(this.validateInputs(this.state.inputs))
    );
  };

  handlePeerChange = peer => {
    this.setState({ to: peer });
  };

  handleInputChange = inputs => {
    const inputsObj = inputs.map(input => JSON.parse(input));
    this.setState(this.validateInputs(inputsObj));
  };

  validateBalance(amount, fee) {
    const balance = getBalance(this.props.peer, this.props.blockchain);
    const totalPaid = amount + fee;
    if (totalPaid > balance) {
      return {
        validateStatusAmountAndFee: "error",
        errorMsgAmountFee: `Недостаточно валюты в кошельке!`,
        amount: amount,
        fee: fee
      };
    } else if (isNaN(amount) || isNaN(fee) || fee === "" || amount === "") {
      return {
        validateStatusAmountAndFee: "error",
        errorMsgAmountFee: "Неверный ввод!",
        amount: amount,
        fee: fee
      };
    } else {
      return {
        validateStatusAmountAndFee: "success",
        errorMsgAmountFee: `Сумма: ${fee + amount}`,
        amount: amount,
        fee: fee
      };
    }
  }

  validateInputs(inputs) {
    const totalPayment = this.state.amount + this.state.fee;
    const inputsTotal = getTotal(inputs);

    if (totalPayment > inputsTotal) {
      return {
        validateStatusInputs: "error",
        errorMsgInputs: `Недостаточно денег`,
        inputs: inputs
      };
    } else {
      return {
        validateStatusInputs: "success",
        errorMsgInputs: `Сумма: ${inputsTotal}`,
        inputs: inputs
      };
    }
  }

  render() {
    const peers = this.props.blockchain.map(blockchain => blockchain.peer);

    const node = this.props.blockchain.find(
      blockchain => blockchain.peer === this.props.peer
    );

    const outputs = getConfirmedUnspentTxOutputs(
      this.props.peer,
      node.blockchain,
      node.transactions
    );

    const { getFieldDecorator } = this.props.form;

    return (
      <Card className="pay-card money-shadow">
        <Form onSubmit={e => this.handleSubmit(e, node.blockchain)}>
          <FormItem>
            <div 
              style={{ 
                fontWeight: "500",
                color: "white",
                marginBottom: "-7px"
              }}>
              Получатель:
            </div>
            {getFieldDecorator("to", {
              rules: [{ required: true, message: "Необходимо выбрать пир" }]
            })(
              <Select
                style={{ width: "120px" }}
                placeholder="Пир"
                notFoundContent="Добавьте пир"
                onChange={this.handlePeerChange}
                size="small"
                defaultActiveFirstOption={false}
              >
                {peers.map((peer, index) => {
                  if (peer !== this.props.peer) {
                    return (
                      <Option key={index} value={peer}>
                        {peer}
                      </Option>
                    );
                  } else {
                    return null;
                  }
                })}
              </Select>
            )}
          </FormItem>
          <FormItem
            validateStatus={this.state.validateStatusAmountAndFee}
            help={
              <span style={{ color: "white" }}>
                {this.state.errorMsgAmountFee}
              </span>
            }
          >
          <div style={{ fontWeight: "500", color: "white" }}>
            Величина / Комиссия:
          </div>
            <InputGroup compact>
              <InputNumber
                style={{ width: "80px" }}
                value={this.state.amount}
                min={0}
                parser={value => value.replace(/\$\s?|(,*)/g, "")}
                onChange={this.handleAmountChange}
                size="small"
              />
              <InputNumber
                style={{ width: "50px" }}
                value={this.state.fee}
                min={0}
                parser={value => value.replace(/\$\s?|(,*)/g, "")}
                onChange={this.handleFeeChange}
                size="small"
              />
            </InputGroup>
          </FormItem>
          <FormItem
            className="inputs-form"
            help={
              <span style={{ color: "white" }}>
                {this.state.errorMsgInputs}
              </span>
            }
            validateStatus={this.state.validateStatusInputs}
            style={{ marginBottom: "20px" }}
          >
            <div
              style={{
                marginBottom: "-7px",
                fontWeight: "500",
                color: "white"
              }}
            >
              Средства:
            </div>
            {getFieldDecorator("inputs", {
              rules: [{ required: true, message: "Выберите средства!" }]
            })(
              <Select
                mode="multiple"
                style={{ width: "150px" }}
                placeholder="Входные средства"
                notFoundContent="Выберите входные значения"
                size="small"
                onChange={this.handleInputChange}
              >
                {outputs.map((output, index) => {
                  return (
                    <Option key={index} value={JSON.stringify(output)}>
                      {output.amount} от {" "}
                      {getInputAddressFromOutput(
                        output.transaction,
                        node.blockchain
                      )}
                    </Option> // address is incorrect, need to go and find the transaction with the input and find the address if NOT fee or reward
                  );
                })}
              </Select>
            )}
          </FormItem>
          <PayButton />
        </Form>
      </Card>
    );
  }
}

const WrappedPay = Form.create()(Pay);

function mapStateToProps(state) {
  return {
    blockchain: state.blockchain,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addTransaction: (transaction, peer) => {
      dispatch(addTransaction(transaction, peer));
    }
  };
}

const PayContainer = connect(mapStateToProps, mapDispatchToProps)(WrappedPay);

export default PayContainer;



// WEBPACK FOOTER //
// ./src/Cards/PayCard/PayCard.js