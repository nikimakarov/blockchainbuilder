import {
  getTotal,
  getUnspentTransactionOutputs,
  filterSpentOutputs
} from "../util.js";

export function createTransaction(peer, to, amount, fee, blockchain, inputs) {
  const outputs = createTransactionOutputs(peer, to, amount, fee, inputs);

  return {
    type: "regular",
    inputs: inputs,
    outputs: outputs
  };
}

function createTransactionOutputs(peer, to, amount, fee, inputs) {
  const inputTotal = getTotal(inputs);
  const outputTotal = amount;
  const changeAmount = inputTotal - outputTotal - fee;

  const outputs = [
    {
      address: to,
      amount: amount
    }
  ];

  if (changeAmount > 0) {
    outputs.push({
      amount: changeAmount,
      address: peer
    });
  }

  return outputs;
}

export function getConfirmedUnspentTxOutputs(peer, blockchain, unconfirmedTxs) {
  const unspentTxOutputs = getUnspentTransactionOutputs(peer, blockchain);
  const unconfirmedTxInputs = getInputs(unconfirmedTxs);

  const confirmedUnspentTxOutputs = filterSpentOutputs(
    unspentTxOutputs,
    unconfirmedTxInputs
  );

  return confirmedUnspentTxOutputs;
}

function getInputs(transactions) {
  const inputs = [];
  transactions.forEach(transaction => {
    transaction.inputs.forEach(input => {
      inputs.push(input);
    });
  });
  return inputs;
}

export function getInputAddressFromOutput(path, blockchain) {
  const blockIndex = path[0];
  const txIndex = path[1];
  const transaction = blockchain[blockIndex].transactions[txIndex];

  if (transaction.type === "reward") {
    return "Майнинга";
  } else if (transaction.type === "fee") {
    return "Комиссионных";
  } else {
    // "regular type"
    return transaction.inputs[0].address;
  }
}


// WEBPACK FOOTER //
// ./src/Cards/PayCard/util.js