import React from "react";
import Button from "antd/lib/button";
import "../../css/Cards/PayCard/PayCard.css";

const PayButton = props => {
  return (
    <Button
      className="pay-button"
      type="primary"
      htmlType="submit"
      icon="pay-circle-o"
    >
      Оплатить
    </Button>
  );
};

export default PayButton;



// WEBPACK FOOTER //
// ./src/Cards/PayCard/PayButton.js