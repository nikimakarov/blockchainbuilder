import crypto from "crypto";
import { getTotal } from "../util.js";

const MINING_REWARD = 100;
const DIFFICULTY = 3;
export const BLOCK_SIZE = 3;

export function generateNextBlock(previousBlock, transactions) {
  const index = previousBlock.index + 1;
  const previousHash = previousBlock.hash;
  let nonce = 0;
  let hash;
  let timestamp;
  // proof-of-work
  do {
    timestamp = new Date().getTime();
    nonce = nonce + 1;
    hash = calculateHash(index, previousHash, timestamp, transactions, nonce);
  } while (!isValidHashDifficulty(hash));

  return {
    index: index,
    previousHash: previousBlock.hash,
    timestamp: timestamp,
    transactions: transactions,
    hash: hash,
    nonce: nonce
  };
}

export function getTransactionsForBlock(transactions, address) {
  addFeesToTransactions(transactions, address);
  transactions.push(createRewardTransaction(address));

  return transactions;
}

export function parseTransactions(transactions) {
  return transactions.map(transaction => JSON.parse(transaction));
}

// export function autoSelectTransactions(transactions) {
//   const sortedTransactions = sortTransactionsByFee(transactions);
//   const maxTransactions = getMaximumTransactions(sortedTransactions);
//   return stringifyTransactions(maxTransactions);
// }

export function deselect(selection, selectedTransactions) {
  return selectedTransactions.filter(transaction => selection !== transaction);
}

// function getMaximumTransactions(transactions) {
//   return transactions.slice(0, BLOCK_SIZE);
// }

// function stringifyTransactions(transactions) {
//   return transactions.map(transaction => JSON.stringify(transaction));
// }

// function sortTransactionsByFee(transactions) {
//   return transactions.sort((txA, txB) => {
//     return getTransactionFee(txB) - getTransactionFee(txA)
//   })
// }

function createRewardTransaction(address) {
  return {
    type: "reward",
    inputs: [],
    outputs: [{
      amount: MINING_REWARD,
      address: address
    }]
  };
}

function createFeeTransaction(transaction, miner) {
  const fee = getTransactionFee(transaction)
  return {
    type: "fee",
    inputs: [],
    outputs: [{
      address: miner,
      amount: fee
    }]
  };
}

export function getTransactionFee(transaction) {
  const inputTotal = getTotal(transaction.inputs);
  const outputTotal = getTotal(transaction.outputs);

  return inputTotal - outputTotal;
}

function addFeesToTransactions(transactions, address) {
  transactions.forEach(transaction =>
    transactions.push(createFeeTransaction(transaction, address))
  );

  return transactions;
}

export function isValidHashDifficulty(hash) {
  for (var i = 0, b = hash.length; i < b; i++) {
    if (hash[i] !== "0") {
      break;
    }
  }
  return i >= DIFFICULTY;
}

export function calculateHash(index, previousHash, timestamp, transactions, nonce) {
  const data =
    index + previousHash + timestamp + JSON.stringify(transactions) + nonce;

  return crypto
    .createHash("sha256")
    .update(data)
    .digest("hex");
}

export function isValidNextBlock(nextBlock, previousBlock) {
  if (previousBlock.index + 1 !== nextBlock.index) {
    return false
  } else if (previousBlock.hash !== nextBlock.previousHash) {
    return false
  } else if (!isValidHashDifficulty(nextBlock.previousHash)) {
    return false
  } else if (!isValidHashDifficulty(nextBlock.hash)) {
    return false;
  }
  return true;
}

export function isValidChain(chain, genesisBlock) {
  if (JSON.stringify(chain[0]) !== JSON.stringify(genesisBlock)) {
    return false;
  }

  const tempBlocks = [genesisBlock]
  for (let i = 1; i < chain.length; i = i + 1) {
    if (isValidNextBlock(chain[i], tempBlocks[i - 1])) {
      tempBlocks.push(chain[i]);
    } else {
      return false;
    }
  }
  return true;
}


// WEBPACK FOOTER //
// ./src/Cards/MineCard/util.js