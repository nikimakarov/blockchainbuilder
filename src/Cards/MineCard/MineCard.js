import React, { Component } from "react";
import Card from "antd/lib/card";
import Select from "antd/lib/select";
import Button from "antd/lib/button";
import Icon from "antd/lib/icon";
import PropTypes from "prop-types";
import { addBlock, removeTransaction } from "../../actions.js";
import { connect } from "react-redux";
import Form from "antd/lib/form";
import message from "antd/lib/message";
import {
  generateNextBlock,
  getTransactionsForBlock,
  parseTransactions,
  deselect,
  getTransactionFee
} from "./util.js";
import "../../css/Cards/MineCard/MineCard.css";
import { getBalance } from "../util.js";
const FormItem = Form.Item;
const Option = Select.Option;

class MineCard extends Component {
  state = { selectedTransactions: [] }

  handleMine = () => {
    const { peer, latestBlock, addBlock, removeTransaction } = this.props;
    const { selectedTransactions } = this.state;
    const transactions = parseTransactions(selectedTransactions);
    const transactionsForBlock = getTransactionsForBlock(transactions, peer);
    const nextBlock = generateNextBlock(latestBlock, transactionsForBlock);

    addBlock(nextBlock, peer);
    transactionsForBlock.forEach(transaction => {
      removeTransaction(transaction, peer); // TEST: does this re-render everything and then loop?
    });
    this.setState({ selectedTransactions: [] });
    message.success(
      `Успешно создан Блок ${nextBlock.index} с вознаграждением в 100!`
    );
  };

  handleSelect = selection => {
    const { selectedTransactions } = this.state;
    console.log("handle select called =========>");
    this.setState({
      selectedTransactions: [...selectedTransactions, selection]
    });
  };

  handleDeselect = selection => {
    console.log("handle deselect called =========>");
    const deselectedTx = deselect(selection, this.state.selectedTransactions);
    this.setState({ selectedTransactions: deselectedTx });
  };

  render() {
    const { peer, blockchain, unconfirmedTransactions } = this.props;
    return (
      <Card
        className={`${peer === "Satoshi" ? "mine-1 mine-2" : ""} mine-card money-shadow`}
      >
        <FormItem
          help={<span style={{ color: "white" }}>Вознаграждение: 100</span>}
          style={{
            marginBottom: "10px"
          }}
        >
          <div
            className="unconfirmed-text"
            style={{
              display: `${
                unconfirmedTransactions.length === 0 ? "none" : "block"
              }`
            }}
          >
            Неподтвержденные транзакции:
          </div>
          <Select
            mode="multiple"
            placeholder="Please select transaction to mine"
            className="tx-input"
            notFoundContent="Please make a payment"
            value={this.state.selectedTransactions}
            onSelect={this.handleSelect}
            onDeselect={this.handleDeselect} 
            style={{
              display: `${
                unconfirmedTransactions.length === 0 ? "none" : "block"
              }`
            }}
          >
            {unconfirmedTransactions.map((transaction, index) => (
              <Option key={index} value={JSON.stringify(transaction)}>
                <Icon type="pay-circle-o" /> {transaction.outputs[0].amount}/{getTransactionFee(
                  transaction
                )}{" "}
                {transaction.inputs[0].address} to{" "}
                {transaction.outputs[0].address}
              </Option>
            ))}
          </Select>
        </FormItem>
        <Button
          className={`mine-button ${
            getBalance(peer, blockchain) === 0 ? "animate" : null
          }`}
          onClick={this.handleMine}
          type="primary"
          icon="tool"
        >
          Майнинг нового блока
        </Button>
      </Card>
    );
  }
}

MineCard.propTypes = {
  transactions: PropTypes.array,
  peer: PropTypes.string,
  latestBlock: PropTypes.object,
  addBlock: PropTypes.func,
  removeTransaction: PropTypes.func,
  blockchain: PropTypes.array,
  unconfirmedTransactions: PropTypes.array
};

function mapStateToProps(state) {
  return {
    blockchain: state.blockchain
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addBlock: (block, peer) => {
      dispatch(addBlock(block, peer));
    },
    removeTransaction: (transaction, peer) => {
      dispatch(removeTransaction(transaction, peer));
    }
  };
}

const MineContainer = connect(mapStateToProps, mapDispatchToProps)(MineCard);

export default MineContainer;



// WEBPACK FOOTER //
// ./src/Cards/MineCard/MineCard.js