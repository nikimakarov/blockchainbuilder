import React from 'react';
import BalanceCard from './BalanceCard/BalanceCard.js';
import PayCard from './PayCard/PayCard.js';
import MineCard from './MineCard/MineCard.js';
import  "../css/Cards/Cards.css";

function Cards(props) {
  return (
    <div className="cards">
      <MineCard
        peer={props.peer}
        latestBlock={props.latestBlock}
        unconfirmedTransactions={props.unconfirmedTransactions}
        sendLatestBlock={props.sendLatestBlock}
      />
      <BalanceCard
        peer={props.peer}
      />
      <PayCard
        peer={props.peer}
      />
    </div>
  );
};

export default Cards;


// WEBPACK FOOTER //
// ./src/Cards/Cards.js