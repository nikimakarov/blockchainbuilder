/**
 * Module Dependencies
 */
const config  = require('./config'),
      restify = require('restify'),
      sha256 = require('sha256'),
      neo4j = require('neo4j-driver');

// var db = new neo4j.GraphDatabase({
//     url: 'http://neo4j:123456@localhost:7474'
// });
var driver = neo4j.v1.driver("bolt://localhost:7687", neo4j.v1.auth.basic("neo4j", "123456"));
var session = driver.session();
/**
 * Initialize Server
 */
const server = restify.createServer({
    name    : config.name,
    version : config.version,
    url : config.hostname
});
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser({
  requestBodyOnGet: true
}));

server.post('/block', function(req,res){
  res.setHeader('Access-Control-Allow-Origin', '*');
  var blockchain = JSON.parse(req.body)[0].blockchain;
  console.log(blockchain);
  setBlocks(blockchain);
  res.send(200);
});

server.post('/', function(req,res){
  res.setHeader('Access-Control-Allow-Origin', '*');
  session
  .run('MATCH (a:block) RETURN count(a:block)')
  .then(result => {
    let existingBlocksNumber = result.records[0]._fields[0].low;
    if (Number(existingBlocksNumber) == 0){
      blockchainInit();
    }
    session.close();
    var blockchain = JSON.parse(req.body)[0].blockchain;
    var slicedBlockchain = blockchain.slice(existingBlocksNumber);
    console.log(existingBlocksNumber);
    console.log(slicedBlockchain);
    setBlocks(slicedBlockchain);
    res.send(200);
  })
  .catch(error => {
    console.log(error);
  });
});

function blockchainInit(){
    session
    .run('CREATE CONSTRAINT ON (b:block) ASSERT b.hash IS UNIQUE')
    .then(result => {
      console.log(result);
      session.close();
    })
    .catch(error => {
      console.log(error);
    });
  
    session
    .run('CREATE CONSTRAINT ON (t:tx) ASSERT t.txid IS UNIQUE')
    .then(result => {
      console.log(result);
      session.close();
    })
    .catch(error => {
      console.log(error);
    });
  
    session
    .run('CREATE CONSTRAINT ON (o:output) ASSERT o.index IS UNIQUE')
    .then(result => {
      console.log(result);
      session.close();
    })
    .catch(error => {
      console.log(error);
    });
  
    session
    .run('CREATE INDEX ON :block(height)')
    .then(result => {
      console.log(result);
      session.close();
    })
    .catch(error => {
      console.log(error);
    });
}

function setBlocks(blockchain){
  function calculateBlockReward(blockcount) {
    // set the block rewards array
    let reward = 100;
    let i = 1;
    let blockrewards = [];
    while (reward >= 1) {
        blockrewards[i] = reward;
        reward = Math.floor(reward/2);
        i++;
    }
    // work out the block reward level
    let block = 0;
    let level = 0;
    while (blockcount >= block) {
        block += 100;
        level += 1;
    }
    // return the value in satoshis
    return blockrewards[level];
  }

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }


  // blockchain = [
  // {hash:"000231f48b131b1c721b508434cdbf308b0ae7c1a7f6a5ed7db56a34c76c9530",
  // index:0,
  // nonce:2176,
  // previousHash:"0",
  // timestamp:1508270000000,
  // transactions: []},
  //   { index: 1,
  //     previousHash:
  //      '000231f48b131b1c721b508434cdbf308b0ae7c1a7f6a5ed7db56a34c76c9530',
  //     timestamp: 1528924258410,
  //     transactions: [ {type: "regular", inputs: [{transaction: [], index: 0, amount: 100, address: "Gustav"}], outputs: [
  //       {address: "Larry", amount: 4},{amount: 94, address: "Gustav"}]},
  //                     {type: "regular", inputs: [{transaction: [], index: 0, amount: 100, address: "Gustav"}], outputs: [{address: "Larry", amount: 45},
  //                     {amount: 53, address: "Gustav"}]},
  //                     {type: "fee", inputs: [], outputs: [
  //                       {address: "Gustav", amount: 2}]},
  //                     {type: "fee", inputs: [], outputs: [
  //                       {address: "Gustav", amount: 2}]},
  //                     {type: "reward", inputs: [], outputs: [
  //                       {amount: 100, address: "Gustav"}]}],
  //     hash:
  //      '0004f1d5a331492584765c7044e0dbe317cbdd02467141101b6d16e823019bd8',
  //     nonce: 7323 }
  // ];

//   blockchain = [
//   {hash:"000231f48b131b1c721b508434cdbf308b0ae7c1a7f6a5ed7db56a34c76c9530",
//   index:0,
//   nonce:2176,
//   previousHash:"0",
//   timestamp:1508270000000,
//   transactions: []},

//   {hash:"000680b53a7dde671f54b7b5842df184804b4f2e37cf6bdcf901a274ae6212b8",
//   index:1,
//   nonce:1366,
//   previousHash:"000231f48b131b1c721b508434cdbf308b0ae7c1a7f6a5ed7db56a34c76c9530",
//   timestamp:1528753077207,
//   transactions: [
//     {type: "reward", inputs: [], outputs: [{amount: 100, address: "Sato"}]}
//   ]},

//   {hash:"0003683b70610a6631b5f2193cd5cbce2974d1df5a2127060bebfb1358b58e4b",
//   index:2,
//   nonce:1432,
//   previousHash:"000680b53a7dde671f54b7b5842df184804b4f2e37cf6bdcf901a274ae6212b8",
//   timestamp:1528753080999,
//   transactions: [
//     {type: "reward", inputs: [], outputs: [{amount: 100, address: "Gusta"}]}
//   ]},

//   {hash:"0004bce522524de3eee187c2a5a827830959627caf6aa1adb01e8ac6ae0d9a03",
//   index:3,
//   nonce:2456,
//   previousHash:"0003683b70610a6631b5f2193cd5cbce2974d1df5a2127060bebfb1358b58e4b",
//   timestamp:1528753097811,
//   transactions: [
//     {type: "regular", inputs: [{transaction: [7, 0, 0], index: 0, amount: 100, address: "Gustav"}], 
//                       outputs: [{address: "Larry", amount: 4},{amount: 94, address: "Gustav"}]},
//     {type: "fee", inputs: [], outputs: [{address: "Gustav", amount: 2}]},
//     {type: "reward", inputs: [], outputs: [{amount: 100, address: "Gustav"}]}]
//   }
// ]


  //GET blockchain var
  blockchain.forEach(function(block, i, arr) {
    // a. Create the new block, or add properties to it 
    // if we've already made a placeholder for it.
    let createblock = `MERGE (block :block {hash:'${block.hash}'})
    CREATE UNIQUE (block)-[:coinbase]->(:output:coinbase)
    SET
        block.index = ${block.index},
        block.prevblock ='${block.previousHash}',
        block.time=${block.timestamp},
        block.nonce=${block.nonce},
        block.height = 0`;

    let createchain = "";
    // IF GENESIS BLOCK - Do not try and create a chain to previous block
    if (block.previousHash == '0') {
        createchain = `
        SET block.height=0
        RETURN block.height as height, block.prevblock as prevblock;
        `;
    }
    // NOT GENESIS BLOCK - Create chain to previous block (sets placeholder if we haven't got it, because blocks in blk.dat files are not always in order of height)
    else {
        createchain = `
        MERGE (prevblock :block {hash:'${block.previousHash}'})
        MERGE (block)-[:chain]->(prevblock)
        SET block.height=prevblock.height+1
        RETURN block.height as height, block.prevblock as prevblock
        `;
    }
    session
    .run(createblock + createchain)
    .then(result => {
      console.log(result);
      session.close();
    })
    .catch(error => {
      console.log(error);
    });
  

    // If we have a height for this block, set value for coinbase input.
    let height = i;
    let blockreward = 0;
    let newQuery = "";
    if (height !== 0) {
        blockreward = calculateBlockReward(height);
        newQuery =`MATCH (block :block {hash:'${block.hash}'})-[:coinbase]->(coinbase :output:coinbase)
        SET coinbase.value=${blockreward}`;
        session.run(newQuery)
        .then(result => {
          console.log(result);
          session.close();
        })
        .catch(error => {
          console.log(error);
        });
    }

    function makeString() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var n = 0; n < getRandomInt(10,50); n++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    
      return text;
    }


    // add coinbase transaction
    let coinbasetxid = sha256(block.hash + '0' + 'coinbase');
    let coinbaseVersion = 1;
    let coinbaseLocktime = getRandomInt(0,100);
    let coinbaseSize = getRandomInt(0,1000);
    let connectTxToTxId = `MATCH (tx :tx {txid:'${coinbasetxid}'}) RETURN tx`;
    let coinbaseMatchQuery = `
       MATCH (block :block {hash:'${block.hash}'})-[:coinbase]->(coinbase :output:coinbase)
		   MERGE (tx :tx {txid:'${coinbasetxid}', version:${coinbaseVersion}, locktime:${coinbaseLocktime}, size:${coinbaseSize}}) 
       WITH tx, block, coinbase
       `;
      
    session
    .run(connectTxToTxId)
    .then(result => {
      console.log(result);
      session.close();
    })
    .catch(error => {
      console.log(error);
    });
  

    console.log('coinbaseMatchQuery:');
    console.log(coinbaseMatchQuery);
    let scriptSig = makeString();
    let sequence = block.index * 1000;
    let coinbaseMergeQuery = `
    MERGE (coinbase)-[:in {vin:${i}, scriptSig:'${scriptSig}', sequence:'${sequence}'}]->(tx) `;

    session
    .run(coinbaseMatchQuery + coinbaseMergeQuery)
    .then(result => {
      console.log(result);
      session.close();
    })
    .catch(error => {
      console.log(error);
    });
  

    console.log('coinbaseMergeQuery:');
    console.log(connectTxToTxId + coinbaseMatchQuery + coinbaseMergeQuery);

    //CHECK NEXT
    // add other transactions

    block.transactions.forEach(function(transaction, transactionIndex, arr) {
      // $t is i
      // $transaction is transaction
      // $blockhash is hash

      let txid = sha256(block.hash + transactionIndex + transaction.type);
      let connectTxToTxId = `MATCH (tx :tx {txid:'${txid}'}) RETURN tx`;

      session
      .run(connectTxToTxId)
      .then(result => {
        console.log(result);
        session.close();
      })
      .catch(error => {
        console.log(error);
      });

      let hash = block.hash;
      let version = 1;
      let locktime = getRandomInt(0,100);
      let size = getRandomInt(0,1000);

      let txNodeCypher = `
       MATCH (block :block {hash:'${hash}'})-[:coinbase]->(coinbase :output:coinbase)
		   MERGE (tx :tx {txid:'${txid}', type: '${transaction.type}', version:${version}, locktime:${locktime}, size:${size}}) 
       WITH tx, block, coinbase
       `;

      let inputCypher = "";
      transaction.inputs.forEach(function(inputVal, vin, arr) {
        //vin; input.index; input.amount; input.address
        let sequence = block.index * 1000 + i + vin;
        let scriptSig = makeString();
        let witness =  sha256(vin + inputVal.index + scriptSig + sequence);
        let input = {vin:vin, index: inputVal.index, value: inputVal.amount, address: inputVal.address,
          scriptSig: scriptSig, sequence: sequence, witness: witness};

        inputCypher += `
          MERGE (in${vin} :output {index: ${input.index}}) 
          MERGE (in${vin})-[:in {vin: ${input.vin}, scriptSig: '${input.scriptSig}', sequence: ${input.sequence}, witness: '${input.witness}',
          value: ${input.value}, address: '${input.address}'}]->(tx)
        `;
      });

      let outputCypher = "";
      transaction.outputs.forEach(function(outputVal, vout, arr) {
        // vout; output.address; output.amount
        let scriptPubKey = makeString();
        let output = {vout:vout, index: `${txid}:${vout}`, value: outputVal.amount, scriptPubKey: scriptPubKey,
          address: outputVal.address};
        let num = `${vout}`;
        outputCypher += `
          MERGE (out${num} :output {index: '${output.index}'})
          MERGE (tx)-[:out {vout: ${output.vout}}]->(out${num})
          MERGE (address${num} :address {address: '${output.address}'})
          MERGE (out${num})-[:locked]->(address${num})
          MERGE (out${num})-[:in]->(existing)
          ON CREATE SET
            out${num}.value= ${output.value},
            out${num}.scriptPubKey= '${output.scriptPubKey}'
          ON MATCH SET
            out${num}.value= ${output.value},
            out${num}.scriptPubKey= '${output.scriptPubKey}',
            existing.fee = existing.fee + ${output.value}
        `;
      });

      let blockMergeCypher = `
      MERGE (tx)-[:inc {i:${transactionIndex}}]->(block)
      `;

      session.run(txNodeCypher + inputCypher + outputCypher + blockMergeCypher)
      .then(result => {
        console.log(result);
        session.close();
      })
      .catch(error => {
        console.log(error);
      });
    

      console.log('CHECK:');
      console.log(txNodeCypher + inputCypher + outputCypher + blockMergeCypher);

    });

  });
};

server.listen(3010, function () {
  console.log('%s listening at %s', server.name, server.url);
});