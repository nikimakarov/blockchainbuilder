var neo4j = require('neo4j');

var db = new neo4j.GraphDatabase({
    url: 'http://localhost:7474'
});

function calculateBlockReward(blockcount) {
    // set the block rewards array
    let reward = 5000000000;
    let i = 1;
    let blockrewards = [];
    while (reward >= 1) {
        blockrewards[i] = reward;
        reward = floor(reward/2);
        i++;
    }
    // work out the block reward level
    let block = 0;
    let level = 0;
    while (blockcount >= block) {
        block += 210000;
        level += 1;
    }
	
	// return the value in satoshis
	return blockrewards[level];
}

//BLOCK MAKING
db.cypher({
    query: "CREATE CONSTRAINT ON (b:block) ASSERT b.hash IS UNIQUE",
}, callback);

db.cypher({
    query: "CREATE CONSTRAINT ON (t:tx) ASSERT t.txid IS UNIQUE",
}, callback);

db.cypher({
    query: "CREATE CONSTRAINT ON (o:output) ASSERT o.index IS UNIQUE",
}, callback);

db.cypher({
    query: "CREATE INDEX ON :block(height)",
}, callback);

blockchain = [
{'hash':"000231f48b131b1c721b508434cdbf308b0ae7c1a7f6a5ed7db56a34c76c9530",
'index':0,
'nonce':2176,
'previousHash':"0",
'timestamp':1508270000000},

{'hash':"000680b53a7dde671f54b7b5842df184804b4f2e37cf6bdcf901a274ae6212b8",
'index':1,
'nonce':1366,
'previousHash':"000231f48b131b1c721b508434cdbf308b0ae7c1a7f6a5ed7db56a34c76c9530",
'timestamp':1528753077207},

{'hash':"0003683b70610a6631b5f2193cd5cbce2974d1df5a2127060bebfb1358b58e4b",
'index':2,
'nonce':2456,
'previousHash':"000680b53a7dde671f54b7b5842df184804b4f2e37cf6bdcf901a274ae6212b8",
'timestamp':1528753097811}
]
//GET blockchain var
blockchain.forEach(function(block, i, arr) {
    // a. Create the new block, or add properties to it 
    // if we've already made a placeholder for it.
    let createblock = `MERGE (block:block {hash:'${block.hash}'})
    CREATE UNIQUE (block)-[:coinbase]->(:output:coinbase)
    SET
        block.index = ${block.index},
        block.prevblock ='${block.previousHash}',
        block.time=${block.timestamp},
        block.nonce=${block.nonce}`;

    let createchain = "";
    // IF GENESIS BLOCK - Do not try and create a chain to previous block
    if (block.previousHash == '0000000000000000000000000000000000000000000000000000000000000000') {
        createchain = `
        SET block.height=0
        RETURN block.height as height, block.prevblock as prevblock
        `;
    }
    // NOT GENESIS BLOCK - Create chain to previous block (sets placeholder if we haven't got it, because blocks in blk.dat files are not always in order of height)
    else {
        createchain = `
        MERGE (prevblock:block {hash:'${block.previousHash}'})
        MERGE (block)-[:chain]->(prevblock)
        SET block.height=prevblock.height+1
        RETURN block.height as height, block.prevblock as prevblock
        `;
    }
    let query = createblock + " " + createchain;

    db.cypher({
        query: query,
    }, callback);

    // If we have a height for this block, set value for coinbase input.
    let height = i;
    let blockreward = 0;
    if (height !== 0) {
        blockreward = calculateBlockReward(height);
        let newQuery =`MATCH (block :block {hash:'${block.hash}'})-[:coinbase]->(coinbase :output:coinbase)
        SET coinbase.value=${blockreward}`;
        db.cypher({
            query: newQuery,
        }, callback);
    }
});

// ORPHAN & TRANSACTIONS


// server.get('/constraints', function (req, res) {
//   db.cypher({
//     query: 'CREATE CONSTRAINT ON (b:block) ASSERT b.hash IS UNIQUE'
//   }, function (err, results) {
//     if (err) res.end(err);
//     res.send(200);
//   });
// });

/*server.get('/echo/:name', function (req, res, next) {
  res.send(req.params);
  return next();
});*/

// //rest api to get all results
// server.get('/employees', function (req, res) {
//    connection.query('select * from employee', function (error, results, fields) {
// 	  if (error) throw error;
// 	  res.end(JSON.stringify(results));
// 	});
// });

// //rest api to get a single employee data
// server.get('/employees/:id', function (req, res) {
//    connection.query('select * from employee where id=?', [req.params.id], function (error, results, fields) {
// 	  if (error) throw error;
// 	  res.end(JSON.stringify(results));
// 	});
// });

// //rest api to create a new record into mysql database
// server.post('/employees', function (req, res) {
//    var postData  = req.body;
//    connection.query('INSERT INTO employee SET ?', postData, function (error, results, fields) {
// 	  if (error) throw error;
// 	  res.end(JSON.stringify(results));
// 	});
// });

// //rest api to update record into mysql database
// server.put('/employees', function (req, res) {
//    connection.query('UPDATE `employee` SET `employee_name`=?,`employee_salary`=?,`employee_age`=? where `id`=?', [req.body.employee_name,req.body.employee_salary, req.body.employee_age, req.body.id], function (error, results, fields) {
// 	  if (error) throw error;
// 	  res.end(JSON.stringify(results));
// 	});
// });

// //rest api to delete record from mysql database
// server.delete('/employees/:id', function (req, res) {
//    connection.query('DELETE FROM `employee` WHERE `id`=?', [req.params.id], function (error, results, fields) {
//     if (error) throw error;
//     res.end('Record has been deleted!');
//   });
// });